<?php $settings = get_settings(); ?>

<footer id="footer" class="clearfix dark">
    
    <!-- .footer start -->
    <!-- ================ -->
    <div class="footer">
        <div class="container">
            <div class="footer-inner">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="footer-content" style="padding: 0px 0px !important;">
                            <div class="logo-footer"><img id="logo-footer" src="<?php echo base_url("panel/uploads/settings_v/150x35/$settings->logo"); ?>" alt="<?php echo base_url($settings->company_name);?>"></div>
                            <p>
                                <?php echo $settings->mission ; ?>
                            </p>
                            <ul class="list-inline mb-20">
                                <li class="list-inline-item"><i class="text-default fa fa-map-marker pr-1"></i> <?php echo strip_tags($settings->address); ?></li>
                                <li class="list-inline-item"><i class="text-default fa fa-phone pl-10 pr-1"></i> <?php echo $settings->phone_1; ?></li>
                                <li class="list-inline-item"><a href="mailto:<?php echo $settings->email; ?>" class="link-dark"><i class="text-default fa fa-envelope-o pl-10 pr-1"></i> <?php echo $settings->email; ?></a></li>
                            </ul>
                            <div class="separator-2"></div>
                            <ul class="social-links circle margin-clear animated-effect-1">
                                
                                <?php if(!empty($settings->facebook)){ ?>
                                    <li class="facebook"><a target="_blank" href="https://www.facebook.com<?php echo $settings->facebook; ?>"><i class="fa fa-facebook"></i></a></li>
                                <?php } ?>
                                <?php if(!empty($settings->twitter)){ ?>
                                    <li class="twitter"><a target="_blank" href="https://twitter.com<?php echo $settings->twitter; ?>"><i class="fa fa-twitter"></i></a></li>
                                <?php } ?>
                                <?php if(!empty($settings->instagram)){ ?>
                                    <li class="instagram"><a target="_blank" href="https://www.instagram.com<?php echo $settings->instagram; ?>"><i class="fa fa-instagram"></i></a></li>
                                <?php } ?>
                                <?php if(!empty($settings->linkedin)){ ?>
                                    <li class="linkedin"><a target="_blank" href="https://www.linkedin.com/in<?php echo $settings->linkedin; ?>"><i class="fa fa-linkedin"></i></a></li>
                                <?php } ?>
                                <?php if(!empty($settings->github)){ ?>
                                    <li class="github"><a target="_blank" href="https://github.com<?php echo $settings->github; ?>"><i class="fa fa-github"></i></a></li>
                                <?php } ?>
                                <?php if(!empty($settings->googleplus)){ ?>
                                    <li class="googleplus"><a target="_blank" href="https://plus.google.com<?php echo $settings->googleplus; ?>"><i class="fa fa-google-plus"></i></a></li>
                                <?php } ?>

                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-6" style="padding-top: 60px;">
                        <div>
                            <iframe
                                width="100%"
                                height="380px"
                                frameborder="0"
                                scrolling="no"
                                marginheight="0"
                                marginwidth="0"
                                src="https://www.openstreetmap.org/export/embed.html?bbox=<?php echo $settings->bbox; ?>&amp;layer=mapnik&amp;marker=<?php echo $settings->latitude; ?>%2C<?php echo $settings->longtitude; ?>"
                            >
                            </iframe>
                            <br/>
                            <small>
                                <a target="_blank" href="https://www.openstreetmap.org/?mlat=<?php echo $settings->latitude; ?>&amp;mlon=<?php echo $settings->longtitude; ?>#map=15/<?php echo $settings->latitude; ?>/<?php echo $settings->longtitude; ?>&zoom=10">
                                    Daha Büyük Haritayı Göster
                                </a>
                            </small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .footer end -->
    
    <!-- .subfooter start -->
    <!-- ================ -->
    <div class="subfooter">
        <div class="container">
            <div class="subfooter-inner">
                <div class="row">
                    <div class="col-md-12">
                        <p class="text-center">Tüm Hakları Saklıdır © <?php echo date("Y"); ?> <span><img style="display: inline" width="90px" src="<?php echo base_url("panel/uploads/settings_v/90x21/$settings->logo"); ?>" alt="<?php echo $settings->company_name; ?>"></span>  by <a class="signature" target="_blank" href="https://muratyurur.com">Murat Yürür</a>. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .subfooter end -->

</footer>
<!-- footer end -->