<meta charset="utf-8">
<title>Beauty Center</title>
<meta name="description" content="Güzellik Merkezi Scripti">
<meta name="author" content="Murat Yürür">

<!-- Mobile Meta -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<?php if (isset($opengraph)) { ?>
    
    <meta property="og:title" content="<?php echo $news->title; ?>">
    <meta property="og:description" content="<?php echo strip_tags($news->description); ?>" />
    
    <?php if ($news->news_type == "image") { ?>
        
        <meta property="og:image" content="<?php echo base_url("panel/uploads/news_v/$news->img_url"); ?>" />
    
    <?php } else { ?>
        
        <meta property="og:video" content="<?php echo "https://www.youtube.com/v/$news->video_url"; ?>" />
    
    <?php } ?>
    
<?php } ?>


<?php $this->load->view("includes/include_style"); ?>