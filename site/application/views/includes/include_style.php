<?php $settings = get_settings(); ?>
<!-- Favicon -->
<link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url("panel/uploads/settings_v/192x192/$settings->favicon"); ?>">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url("panel/uploads/settings_v/192x192/$settings->favicon"); ?>">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url("panel/uploads/settings_v/192x192/$settings->favicon"); ?>">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url("panel/uploads/settings_v/192x192/$settings->favicon"); ?>">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url("panel/uploads/settings_v/192x192/$settings->favicon"); ?>">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url("panel/uploads/settings_v/192x192/$settings->favicon"); ?>">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url("panel/uploads/settings_v/192x192/$settings->favicon"); ?>">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url("panel/uploads/settings_v/192x192/$settings->favicon"); ?>">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url("panel/uploads/settings_v/192x192/$settings->favicon"); ?>">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url("panel/uploads/settings_v/192x192/$settings->favicon"); ?>">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url("panel/uploads/settings_v/192x192/$settings->favicon"); ?>">
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url("panel/uploads/settings_v/192x192/$settings->favicon"); ?>">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url("panel/uploads/settings_v/192x192/$settings->favicon"); ?>">
<link rel="manifest" href="<?php echo base_url("assets");?>/images/favicon/site.webmanifest">
<link rel="mask-icon" href="<?php echo base_url("assets");?>/images/favicon/safari-pinned-tab.svg" color="#ab6cc6">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="theme-color" content="#ffffff">

<!-- Web Fonts -->
<link href="https://fonts.googleapis.com/css?family=PT+Serif|Pacifico|Raleway:300,400,700|Roboto:300,300i,400,400i,500,500i,700,700i" rel="stylesheet">

<!-- Bootstrap core CSS -->
<link href="<?php echo base_url("assets");?>/bootstrap/css/bootstrap.css" rel="stylesheet">

<!-- Font Awesome CSS -->
<link href="<?php echo base_url("assets");?>/fonts/font-awesome/css/font-awesome.css" rel="stylesheet">

<!-- Fontello CSS -->
<link href="<?php echo base_url("assets");?>/fonts/fontello/css/fontello.css" rel="stylesheet">

<!-- Plugins -->
<link href="<?php echo base_url("assets");?>/plugins/magnific-popup/magnific-popup.css" rel="stylesheet">
<link href="<?php echo base_url("assets");?>/plugins/rs-plugin-5/css/settings.css" rel="stylesheet">
<link href="<?php echo base_url("assets");?>/plugins/rs-plugin-5/css/layers.css" rel="stylesheet">
<link href="<?php echo base_url("assets");?>/plugins/rs-plugin-5/css/navigation.css" rel="stylesheet">
<link href="<?php echo base_url("assets");?>/css/animations.css" rel="stylesheet">
<link href="<?php echo base_url("assets");?>/plugins/owlcarousel2/assets/owl.carousel.min.css" rel="stylesheet">
<link href="<?php echo base_url("assets");?>/plugins/owlcarousel2/assets/owl.theme.default.min.css" rel="stylesheet">
<link href="<?php echo base_url("assets");?>/plugins/hover/hover-min.css" rel="stylesheet">

<!-- The Project's core CSS file -->
<!-- Use css/rtl_style.css for RTL version -->
<link href="<?php echo base_url("assets");?>/css/style.css" rel="stylesheet" >
<!-- The Project's Typography CSS file, includes used fonts -->
<!-- Used font for body: Roboto -->
<!-- Used font for headings: Raleway -->
<!-- Use css/rtl_typography-default.css for RTL version -->
<link href="<?php echo base_url("assets");?>/css/typography-default.css" rel="stylesheet" >
<!-- Color Scheme (In order to change the color scheme, replace the blue.css with the color scheme that you prefer)-->
<link href="<?php echo base_url("assets");?>/css/skins/purple.css" rel="stylesheet">


<!-- Custom css -->
<link href="<?php echo base_url("assets");?>/css/custom.css" rel="stylesheet">