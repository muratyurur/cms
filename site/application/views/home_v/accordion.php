<section class="section pv-40 clearfix">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="title text-center"><strong class="text-default logo-font"><?php echo $settings->company_name; ?></strong> biz kimiz?</h3>
            </div>
            <div class="col-lg-6">
                <!-- accordion start -->
                <!-- ================ -->
                <div id="accordion-2" class="collapse-style-2 mb-4" role="tablist" aria-multiselectable="true">
                    <div class="card">
                        <div class="card-header" role="tab" id="headingOne-2">
                            <h4 class="mb-0">
                                <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseOne-2"
                                   aria-expanded="true" aria-controls="collapseOne-2">
                                    <i class="fa fa-bullseye pr-10"></i>Misyonumuz
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne-2" class="collapse show" role="tabpanel" aria-labelledby="headingOne-2">
                            <div class="card-block">
                                <?php echo character_limiter(strip_tags($settings->mission), 700); ?>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" role="tab" id="headingTwo-2">
                            <h4 class="mb-0">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion-2"
                                   href="#collapseTwo-2" aria-expanded="false" aria-controls="collapseTwo-2">
                                    <i class="fa fa-eye pr-10"></i>Vizyonumuz
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwo-2" class="collapse" role="tabpanel" aria-labelledby="headingTwo-2">
                            <div class="card-block">
                                <?php echo character_limiter(strip_tags($settings->vision), 700); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- accordion end -->
            </div>
            <div class="col-lg-6">
                <div class="owl-carousel content-slider-with-controls">
                    <?php foreach ($images as $image) { ?>
                    <div class="overlay-container overlay-visible">
                        <img src="<?php echo base_url("panel/uploads/imageGallery_v/$imageGallery->folder_name/540x334/$image->image_url"); ?>"
                             alt="<?php echo $settings->company_name; ?>">
                        <div class="overlay-bottom hidden-sm-down">
                            <div class="text">
                                <h3 class="title"><?php echo $settings->company_name . " | " . $imageGallery->gallery_name;; ?></h3>
                            </div>
                        </div>
                        <a href="<?php echo base_url("panel/uploads/imageGallery_v/$imageGallery->folder_name/750x464/$image->image_url"); ?>"
                           class="owl-carousel--popup-img overlay-link" title="<?php echo $imageGallery->gallery_name; ?>"><i class="icon-plus-1"></i></a>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>