<!-- banner start -->
<!-- ================ -->
<div class="banner dark-translucent-bg" style="background-image:url('<?php echo base_url("panel/uploads/settings_v/who-we-are-banner.png"); ?>'); background-position: 50% 27%;">
     <div class="container">
        <div class="row justify-content-lg-center">
            <div class="col-md-12 text-center pv-20">
                <h3 class="title logo-font object-visible" data-animation-effect="fadeIn" data-effect-delay="100"><span class="text-default">Beauty</span> Center</h3>
                <div class="separator object-visible mt-10" data-animation-effect="fadeIn" data-effect-delay="100"></div>
                <p class="text-center object-visible" data-animation-effect="fadeIn" data-effect-delay="100"><?php echo character_limiter(strip_tags($settings->about_us), 250); ?></p>
            </div>
        </div>
    </div>
</div>
<!-- banner end -->

<!-- main-container start -->
<!-- ================ -->
<section class="main-container padding-bottom-clear">

    <div class="container">
        <div class="row">

            <!-- main start -->
            <!-- ================ -->
            <div class="main col-12">
                <h3 class="title">Biz <strong>Kimiz?</strong></h3>
                <div class="separator-2"></div>
                <div class="row">
                    <div class="col-lg-12">
                        <p><?php echo $settings->about_us; ?></p>
                    </div>
                </div>
                <div class="separator-2"></div>
            </div>
            <!-- main end -->

            <!-- section start -->
            <!-- ================ -->
            <div class="section">
                <div class="container">
                    <h3 class="mt-4">Neden bizi <strong>seçmelisiniz?</strong></h3>
                    <div class="separator-2"></div>
                    <div class="row">
                        <!-- accordion start -->
                        <!-- ================ -->
                        <!-- accordion start -->
                        <!-- ================ -->
                        <div id="accordion-2" class="collapse-style-2" role="tablist" aria-multiselectable="true">
                            <div class="card">
                                <div class="card-header" role="tab" id="headingOne-2">
                                    <h4 class="mb-0">
                                        <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseOne-2" aria-expanded="true" aria-controls="collapseOne-2">
                                            <i class="fa fa-bullseye pr-10"></i>Misyonumuz
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne-2" class="collapse show" role="tabpanel" aria-labelledby="headingOne-2">
                                    <div class="card-block">
                                        <?php echo $settings->mission; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" role="tab" id="headingTwo-2">
                                    <h4 class="mb-0">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion-2" href="#collapseTwo-2" aria-expanded="false" aria-controls="collapseTwo-2">
                                            <i class="fa fa-eye pr-10"></i>Vizyonumuz
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo-2" class="collapse" role="tabpanel" aria-labelledby="headingTwo-2">
                                    <div class="card-block">
                                        <?php echo $settings->vision; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- section end -->
        </div>
    </div>

</section>
<!-- main-container end -->