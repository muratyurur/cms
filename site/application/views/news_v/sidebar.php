<aside class="col-lg-4 col-xl-3 ml-xl-auto">
    <div class="sidebar">
        <div class="block clearfix">
            <nav>
                <ul class="nav flex-column">
                    <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>">Ana Sayfa</a></li>
                    <li class="nav-item"><a class="nav-link" href="<?php echo base_url("urun-listesi"); ?>">Ürünler</a></li>
                    <li class="nav-item"><a class="nav-link" href="<?php echo base_url("portfolyo-listesi"); ?>">Portfolyo</a></li>
                    <li class="nav-item"><a class="nav-link" href="<?php echo base_url("hakkimizda"); ?>">Hakkımızda</a></li>
                    <li class="nav-item"><a class="nav-link" href="<?php echo base_url("iletisim"); ?>">İletişim</a></li>
                </ul>
            </nav>
        </div>
        <div class="block clearfix">
            <h3 class="title">Diğer Haberler</h3>
            <div class="separator-2"></div>
    
            <?php foreach ($other_news as $o_news) { ?>
            
            <div class="media margin-clear">
                <div class="d-flex pr-2">
                    <div class="overlay-container">
                        
                        <?php if ($o_news->news_type == "video") { ?>
                            
                            <img class="media-object"
                                 src="https://img.youtube.com/vi/<?php echo $o_news->video_url; ?>/maxresdefault.jpg"
                                 alt="<?php echo $o_news->url; ?>">
                        
                        <?php } else { ?>
                        
                            <img class="media-object"
                                 src="<?php echo base_url("panel/uploads/news_v/165x113/$o_news->img_url"); ?>"
                                 alt="<?php echo $o_news->url; ?>">
                            <a href="blog-post.html"
                               class="overlay-link small">
                                <i class="fa fa-link"></i>
                            </a>
    
                        <?php } ?>
                        
                    </div>
                </div>
                <div class="media-body">
                    <h6 class="media-heading"><a href="<?php echo base_url("haber/$o_news->url"); ?>"><?php echo $o_news->title; ?></a></h6>
                    <p class="small margin-clear"><i class="fa fa-calendar pr-10"></i><?php echo get_readable_date($o_news->createdAt); ?></p>
                </div>
            </div>
            <hr>

            <?php } ?>
            
            <div class="text-right space-top">
                <a href="<?php echo base_url("haberler"); ?>" class="link-dark"><i class="fa fa-plus-circle pl-1 pr-1"></i>Daha Fazla Haber</a>
            </div>
        </div>
        <div class="block clearfix">
            <form role="search">
                <div class="form-group has-feedback">
                    <input type="text" class="form-control" placeholder="Haber Ara">
                    <i class="fa fa-search form-control-feedback"></i>
                </div>
            </form>
        </div>
    </div>
</aside>