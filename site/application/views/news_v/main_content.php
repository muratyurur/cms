<div class="main col-lg-8">
    
    <!-- page-title start -->
    <!-- ================ -->
    <h1 class="page-title"><?php echo $news->title; ?></h1>
    <!-- page-title end -->
    
    <!-- blogpost start -->
    <!-- ================ -->
    <article class="blogpost full">
        <header>
            <div class="post-info mb-4">
                    <span class="post-date">
                      <i class="icon-calendar"></i>
                      <span class="month"><?php echo get_readable_date($news->createdAt); ?></span>
                    </span>
                <?php $viewCount = $news->viewCount + 1; ?>
                <span class="comments"><i class="icon-eye"></i> <?php echo $viewCount; ?> kez görüntülendi</span>
            </div>
        </header>
        <div class="blogpost-content">
            
            <?php if ($news->news_type == "image") { ?>
    
            <div class="overlay-container">
                <img src="<?php echo base_url("panel/uploads/news_v/730x451/$news->img_url"); ?>" alt="<?php echo $news->title; ?>">
                <a class="overlay-link popup-img" href="<?php echo base_url("panel/uploads/news_v/1140x785/$news->img_url"); ?>"><i class="fa fa-search-plus"></i></a>
            </div>

            <?php } else { ?>
    
            <div class="embed-responsive embed-responsive-16by9">
    
                <iframe width="100%"
                        height="100%"
                        src="https://www.youtube.com/embed/<?php echo $news->video_url; ?>"
                        frameborder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen>
                </iframe>
                
            </div>
            
            <?php } ?>
            
            <h3 class="my-4"><?php echo $news->title; ?></h3>
            <blockquote><?php echo character_limiter(strip_tags($news->description), 200 ); ?></blockquote>
            <p>
                <?php echo $news->description; ?>
            </p>
            <footer class="clearfix">
                <div class="link pull-right">
                    <ul class="social-links circle small colored clearfix margin-clear text-right animated-effect-1">
                        <li class="twitter">
                            <a class="share-button" target="_blank" href="http://twitter.com/intent/tweet?text=<?php echo $news->title; ?>&url=<?php echo base_url("haber/$news->url");?>"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li class="googleplus">
                            <a class="share-button" target="_blank" href="http://plus.google.com/share?url=<?php echo base_url("haber/$news->url"); ?>"><i class="fa fa-google-plus"></i></a>
                        </li>
                        <li class="facebook">
                            <a class="share-button" target="_blank" href="http://www.facebook.com/sharer.php?u=<?php echo base_url("haber/$news->url"); ?>&t=<?php echo $news->title ?>"><i class="fa fa-facebook"></i></a>
                        </li>
                    </ul>
                </div>
            </footer>
        </div>
    </article>
    <!-- blogpost end -->
    
</div>