<section class="main-container">
    
    <div class="container">
        <div class="row">
            
            <!-- main start -->
            <!-- ================ -->
            <?php $this->load->view("{$viewFolder}/main_content", $data); ?>
            <!-- main end -->
    
            <!-- sidebar start -->
            <!-- ================ -->
            <?php $this->load->view("{$viewFolder}/sidebar", $data); ?>
            <!-- sidebar end -->
        
        </div>
    </div>
</section>
