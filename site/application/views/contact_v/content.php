<?php $settings = get_settings(); ?>
<!-- banner start -->
<!-- ================ -->
<div class="banner dark-translucent-bg" style="background-image:url('<?php echo base_url("panel/uploads/settings_v/beauty-thumb.jpg"); ?>'); background-position: 50% 30%;">
    <div class="container">
        <div class="row justify-content-lg-center">
            <div class="col-lg-8 text-center pv-20">
                <h1 class="page-title text-center">Bize Ulaşın</h1>
                <div class="separator"></div>
                <p class="lead text-center">Sizden haberler almak harika olacak! Bize bir mesaj bırakın ve yardımcı olabileceğimizi düşündüğünüz bir şey isteyin. Sizden haber almak için sabırsızlanıyoruz!</p>
                <ul class="list-inline mb-20 text-center">
                    <li class="list-inline-item"><i class="text-default fa fa-map-marker pl-10 pr-2"></i><?php echo strip_tags($settings->address); ?></li>
                    <li class="list-inline-item"><a href="tel:<?php echo $settings->phone_1; ?>" class="link-dark"><i class="text-default fa fa-phone pl-10 pr-2"></i><?php echo $settings->phone_1; ?></a></li>
                    <li class="list-inline-item"><a href="mailto:<?php echo $settings->email; ?>" class="link-dark"><i class="text-default fa fa-envelope-o pl-10 pr-2"></i><?php echo $settings->email; ?></a></li>
                </ul>
                <div class="separator"></div>
                <ul class="social-links circle animated-effect-1 margin-clear text-center space-bottom">
                    
                    <?php if ($settings->facebook) { ?>
                        <li class="facebook"><a target="_blank" href="http://www.facebook.com<?php echo $settings->facebook; ?>"><i class="fa fa-facebook"></i></a></li>
                    <?php } ?>
    
                    <?php if ($settings->twitter) { ?>
                        <li class="twitter"><a target="_blank" href="http://www.twitter.com<?php echo $settings->twitter; ?>"><i class="fa fa-twitter"></i></a></li>
                    <?php } ?>
    
                    <?php if ($settings->instagram) { ?>
                        <li class="instagram"><a target="_blank" href="http://www.instagram.com<?php echo $settings->instagram; ?>"><i class="fa fa-instagram"></i></a></li>
                    <?php } ?>
    
                    <?php if ($settings->linkedin) { ?>
                        <li class="linkedin"><a target="_blank" href="http://www.linkedin.com<?php echo $settings->linkedin; ?>"><i class="fa fa-linkedin"></i></a></li>
                    <?php } ?>
                    
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- banner end -->
<!-- main-container start -->
<!-- ================ -->
<section class="main-container">
    
    <div class="container">
        <div class="row">
            
            <!-- main start -->
            <!-- ================ -->
            <div class="main col-12 space-bottom">
                <h2 class="title">Bir mesaj bırakın</h2>
                <div class="row">
                    <div class="col-lg-6">
                        <p>Bizimle paylaşmak istediğiniz her türlü bilgiyi aşağıdaki formu kullanarak iletebilirsiniz.</p>
                        <div class="alert alert-success hidden-xs-up" id="MessageSent">
                            Mesajınız tarafımıza ulaştı. Sizinle en kısa sürede iletişime geçeceğiz.
                        </div>
                        <div class="alert alert-danger hidden-xs-up" id="MessageNotSent">
                            Oops! Sanırım bir şeyler ters gitti. Sayfayı yenileyip tekrar dener misiniz?
                        </div>
                        <div class="contact-form">
                            <form id="contact-form" class="margin-clear" role="form" method="post" action="<?php echo base_url("mesaj-gonder"); ?>">
                                <div class="form-group has-feedback">
                                    <label for="name">İsim*</label>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Adınız ve Soyadınız">
                                    <i class="fa fa-user form-control-feedback"></i>
                                </div>
                                <div class="form-group has-feedback">
                                    <label for="email">ePosta*</label>
                                    <input type="email" class="form-control" id="email" name="email" placeholder="ePosta adresiniz">
                                    <i class="fa fa-envelope form-control-feedback"></i>
                                </div>
                                <div class="form-group has-feedback">
                                    <label for="subject">Konu Başlığı*</label>
                                    <input type="text" class="form-control" id="subject" name="subject" placeholder="Mesajınızın başlığı">
                                    <i class="fa fa-navicon form-control-feedback"></i>
                                </div>
                                <div class="form-group has-feedback">
                                    <label for="message">Mesaj*</label>
                                    <textarea class="form-control" rows="6" id="message" name="message" placeholder="Mesajınız"></textarea>
                                    <i class="fa fa-pencil form-control-feedback"></i>
                                </div>
                                <div class="row align-middle">
                                    <div class="col-md-4">
                                        <?php echo $captcha["image"]; ?>
                                    </div>
                                    <div class="col-md-8 py-2">
                                        <input type="text" class="form-control" name="captcha" placeholder="Doğrulama Kodu">
                                    </div>
                                </div>
                                <input
                                        type="hidden"
                                        name="<?php echo $this->security->get_csrf_token_name(); ?>"
                                        value="<?php echo $this->security->get_csrf_hash(); ?>">
                                <button type="submit" class="submit-button btn btn-lg btn-default">Gönder</button>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div>
                            <iframe
                                    width="100%"
                                    height="380px"
                                    frameborder="0"
                                    scrolling="no"
                                    marginheight="0"
                                    marginwidth="0"
                                    src="https://www.openstreetmap.org/export/embed.html?bbox=<?php echo $settings->bbox; ?>&amp;layer=mapnik&amp;marker=<?php echo $settings->latitude; ?>%2C<?php echo $settings->longtitude; ?>"
                            >
                            </iframe>
                            <br/>
                            <small>
                                <a target="_blank" href="https://www.openstreetmap.org/?mlat=<?php echo $settings->latitude; ?>&amp;mlon=<?php echo $settings->longtitude; ?>#map=15/<?php echo $settings->latitude; ?>/<?php echo $settings->longtitude; ?>&zoom=10">
                                    Daha Büyük Haritayı Göster
                                </a>
                            </small>
                        </div>
                    </div>
                </div>
            </div>
            <!-- main end -->
        </div>
    </div>
</section>
<!-- main-container end -->

<!-- section start -->
<!-- ================ -->
<section class="section pv-40 parallax background-img-1 dark-translucent-bg" style="background-position:50% 60%;">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="call-to-action text-center">
                    <div class="row justify-content-lg-center">
                        <div class="col-lg-8">
                            <h2 class="title">Bültenimize abone olun</h2>
                            <p>Bültenimize abone olarak tüm fırsat, etkinlik ve kampanyalarımızdan önce siz haberdar olun!</p>
                            <div class="separator"></div>
                            <form class="form-inline margin-clear d-flex justify-content-center" action="<?php echo base_url("abone-ol"); ?>" method="post">
                                <div class="form-group has-feedback">
                                    <label class="sr-only" for="subscribe2">ePosta adresiniz</label>
                                    <input type="email" class="form-control form-control-lg" id="subscribe2" placeholder="ePosta adresiniz" name="subscribe2" required="">
                                    <input
                                            type="hidden"
                                            name="<?php echo $this->security->get_csrf_token_name(); ?>"
                                            value="<?php echo $this->security->get_csrf_hash(); ?>"
                                    >
                                    <i class="fa fa-envelope form-control-feedback"></i>
                                </div>
                                <button type="submit" class="btn btn-lg btn-gray-transparent btn-animated margin-clear ml-3">Gönder <i class="fa fa-send"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- section end -->