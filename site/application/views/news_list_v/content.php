<section class="main-container">

    <div class="container">
        <div class="row">

            <!-- main start -->
            <!-- ================ -->
            <div class="main col-12">

                <!-- page-title start -->
                <!-- ================ -->
                <h1 class="page-title">Haber Listesi</h1>
                <div class="separator-2"></div>
                <!-- page-title end -->

                <!-- timeline grid start -->
                <!-- ================ -->
                <div class="timeline clearfix">


                    <?php $counter = 0; ?>
                    <?php foreach ($news_list as $news) { ?>

                        <!-- timeline grid item start -->
                        <div class="timeline-item <?php echo (($counter++ % 2) == 0) ? "pull-left" : "pull-right"; ?> ">
                            <!-- blogpost start -->
                            <article
                                    class="blogpost shadow-2 light-gray-bg bordered"
                                <?php echo ($news->news_type == "video") ? 'id= "left_news" data-animation-effect="fadeInUpSmall" data-effect-delay="100"' : ''; ?>>

                                <?php if ($news->news_type == "image") { ?>

                                    <div class="overlay-container text-center">
                                        <a href="<?php echo base_url("haber/$news->url"); ?>">
                                            <img src="<?php echo base_url("panel/uploads/news_v/513x289/$news->img_url"); ?>"
                                                 alt="<?php echo $news->url; ?>"
                                                 width="513px"
                                                 height="289px"
                                            >
                                        </a>
                                    </div>

                                <?php } else { ?>

                                    <div class="overlay-container">
                                        <a href="<?php echo base_url("haber/$news->url"); ?>">
                                            <img style="margin: 0 auto; max-height: 289px; max-width: 513px"
                                                 src="https://img.youtube.com/vi/<?php echo $news->video_url; ?>/maxresdefault.jpg"
                                                 alt="<?php echo $news->url; ?>"
                                                 width="513px"
                                                 height="289px">
                                        </a>
                                    </div>

                                <?php } ?>

                                <header>
                                    <h2>
                                        <a href="<?php echo base_url("haber/$news->url"); ?>"><?php echo $news->title; ?></a>
                                    </h2>
                                    <div class="post-info">
                                            <span class="post-date">
                                              <i class="icon-calendar"></i>
                                              <span class="month"><?php echo get_readable_date($news->createdAt); ?></span>
                                            </span>
                                        <span class="comments"><i class="icon-eye"></i> <?php echo $news->viewCount; ?>
                                            kez görüntülendi</span>
                                        <?php echo ($news->news_type == "video") ? "<span class='comments'><i class='fa fa-video-camera'></i> Video Haber </span>" : ""; ?>
                                    </div>
                                </header>
                                <div class="blogpost-content">
                                    <p><?php echo character_limiter(strip_tags($news->description), 250); ?></p>
                                </div>
                                <footer class="clearfix">
                                    <div class="link pull-right"><i class="icon-link"></i><a
                                                href="<?php echo base_url("haber/$news->url"); ?>">Haber Detayı</a>
                                    </div>
                                </footer>
                            </article>
                            <!-- blogpost end -->
                        </div>
                        <!-- timeline grid item end -->

                    <?php } ?>

                </div>
                <!-- timeline grid end -->

            </div>
            <!-- main end -->

        </div>
    </div>
</section>