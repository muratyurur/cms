<section class="main-container">
    <div class="container">
        <div class="row">
            <div class="main col-12">
                <h1 class="page-title">Eğitim Listesi</h1>
                <div class="separator-2"></div>
                <p class="lead">Aşağıda sizin için seçtiğimiz eğitimlerimizden bazılarını bulabilirsiniz.</p>
                <?php foreach ($courses as $course) { ?>
                <div class="image-box style-3-b">
                    <div class="row">
                        <div class="col-md-6 col-lg-4 col-xl-3">
                            <div style="vertical-align: middle"
                                 class="overlay-container course_list_cover_image">
                                <img src="<?php echo base_url("panel/uploads/course_v/255x158/$course->img_url"); ?>" alt="<?php echo $course->title; ?>">
                                <div class="overlay-to-top">
                                    <p class="small margin-clear"><em><?php echo $course->title; ?></em></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-8 col-xl-9">
                            <div class="body">
                                <h3 class="title">
                                    <a href="course-item.html"><?php echo $course->title; ?></a>
                                </h3>
                                <p class="small mb-10">
                                    <i class="icon-calendar"></i> <?php echo get_readable_date($course->event_date); ?>
                                </p>
                                <div class="separator-2"></div>
                                <p class="mb-10"><?php echo strip_tags(character_limiter($course->description, 100)); ?></p>
                                <a href="<?php echo base_url("egitim-detay/$course->url"); ?>"
                                   class="btn btn-default btn-hvr hvr-shutter-out-horizontal margin-clear">
                                    İncele
                                    <i class="fa fa-arrow-right pl-10"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>
