<div class="main-container">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- banner start -->
                <!-- ================ -->
                <div class="pv-40 banner light-gray-bg">
                    <div class="container clearfix">

                        <!-- slideshow start -->
                        <!-- ================ -->
                        <div class="slideshow">

                            <!-- slider revolution start -->
                            <!-- ================ -->
                            <div class="slider-revolution-5-container">
                                <div id="slider-banner-boxedwidth" class="slider-banner-boxedwidth rev_slider"
                                     data-version="5.0">
                                    <div class="tp-bannertimer"></div>
                                    <ul class="slides">
                                            <li class="text-center" data-transition="slidehorizontal"
                                            data-slotamount="default" data-masterspeed="default"
                                            data-title="<?php echo $service->title; ?>">

                                            <!-- main image -->
                                            <img src="<?php echo (isset($service->img_url))
                                                ? base_url("panel/uploads/service_v/1080x430/$service->img_url")
                                                :  base_url("assets/images/service-item-banner-1.jpg"); ?>"
                                                 alt="<?php echo $service->title; ?>" data-bgposition="center top" data-bgrepeat="no-repeat"
                                                 data-bgfit="cover" class="rev-slidebg" style="height: 450px; width: 1140px;">

                                            <!-- Transparent Background -->
                                            <div class="tp-caption dark-translucent-bg"
                                                 data-x="center"
                                                 data-y="center"
                                                 data-start="0"
                                                 data-transform_idle="o:1;"
                                                 data-transform_in="o:0;s:600;e:Power2.easeInOut;"
                                                 data-transform_out="o:0;s:600;"
                                                 data-width="5000"
                                                 data-height="450">
                                            </div>

                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- slider revolution end -->

                        </div>
                        <!-- slideshow end -->

                    </div>
                </div>
                <!-- banner end -->
                
                <!-- main-container start -->
                <!-- ================ -->
                <section class="main-container padding-ver-clear">
                    <div class="container pv-40">
                        <div class="row">
                            
                            <!-- main start -->
                            <!-- ================ -->
                            <div class="main col-lg-8">
                                <h1 class="title"><?php echo $service->title; ?></h1>
                                <div class="separator-2"></div>
                                <p><?php echo $service->description; ?></p>
                            </div>
                            <!-- main end -->
                        </div>
                    </div>
                </section>
                <!-- main-container end -->
                
                <!-- section start -->
                <!-- ================ -->
                <section class="section light-gray-bg pv-40 clearfix">
                    <div class="container">
                        <h3 class="mt-3">Diğer <strong>Hizmetlerimiz</strong></h3>
                        <div class="row grid-space-10">
    
                            <?php foreach($other_services as $service) { ?>
        
                                <div class="col-sm-4">
                                    <div class="image-box style-2 mb-20 bordered light-gray-bg">
                                        <div class="overlay-container overlay-visible">
                    
                                            <img src="<?php echo base_url("panel/uploads/service_v/350x217/$service->img_url"); ?>"
                                                 alt="<?php echo $service->title; ?>">
                                            <div class="overlay-bottom text-left">
                                                <p class="lead margin-clear"><?php echo $service->title; ?></p>
                                            </div>
                                        </div>
                                        <div class="body">
                                            <p><?php echo character_limiter(strip_tags($service->description), 100); ?></p>
                                            <a href="<?php echo base_url("hizmet-detay/$service->url"); ?>" class="btn btn-default btn-sm btn-hvr hvr-sweep-to-right margin-clear">İncele<i class="fa fa-arrow-right pl-10"></i></a>
                                        </div>
                                    </div>
                                </div>
    
                            <?php } ?>
                            
                        </div>
                    </div>
                </section>
                <!-- section end -->
            </div>
        </div>
    </div>
</div>