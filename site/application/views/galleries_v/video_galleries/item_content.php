<section class="main-container">
    <div class="container">
        <div class="row">
            <div class="main col-md-12">
                <h1 class="page-title"><?php echo $gallery->gallery_name; ?></h1>
                <div class="separator-2"></div>
                <div class="row grid-space-20">
                    <?php if (!empty($videos)) { ?>
                        <?php $index = 1; ?>
                        <?php foreach ($videos as $video) { ?>
                            <div class="col-3 mb-20">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe class="embed-responsive-item"
                                            src="//www.youtube.com/embed/<?php echo $video->video_url; ?>"
                                            allowfullscreen
                                    >
                                    </iframe>
                                </div>
                                <p class="text-center"><?php echo $gallery->gallery_name . " video" . $index++; ?></p>
                            </div>
                        <?php } ?>
                    <?php } else { ?>
                        <div class="alert alert-warning col-md-12 text-center">
                            <div style="font-size: 18px">
                                <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                Bu galeriye ait herhangi bir içerik bulunamadı...
                            </div>
                        </div>
                    <?php } ?>
                    <div class="col-md-12">
                        <a href="<?php echo base_url("video-galerisi"); ?>" class="btn btn-default">
                            <i class="fa fa-chevron-left"></i>
                            Galeri Listesine Dön
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>