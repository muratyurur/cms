<section class="main-container">
    <div class="container">
        <div class="row">
            <div class="main col-md-12">
                <h1 class="page-title"><?php echo $gallery->gallery_name; ?></h1>
                <div class="separator-2"></div>
                <div class="row grid-space-20">
                    <?php if (!empty($files)) { ?>
                        <?php $index = 1; ?>
                            <table class="table table-bordered table-hover table-striped table-colored">
                                <thead>
                                <th>Dosya Adı</th>
                                <th class="text-center" width="100px">İndir</th>
                                </thead>
                                <tbody>
                                <?php foreach ($files as $file) { ?>
                                    <tr>
                                        <td>
                                            <?php echo $file->file_name; ?>
                                        </td>
                                        <td>
                                            <a target="_blank"
                                               href="<?php echo base_url("panel/uploads/fileGallery_v/$gallery->folder_name/$file->file_url"); ?>"
                                               class="btn btn-animated btn-default"
                                            >
                                                İndir
                                                <i class="pl-10 fa fa-download"></i>
                                            </a>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                    <?php } else { ?>
                        <div class="alert alert-warning col-md-12 text-center">
                            <div style="font-size: 18px">
                                <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                Bu galeriye ait herhangi bir içerik bulunamadı...
                            </div>
                        </div>
                    <?php } ?>
                    <div class="col-md-12">
                        <a href="<?php echo base_url("dosya-galerisi"); ?>" class="btn btn-default">
                            <i class="fa fa-chevron-left"></i>
                            Galeri Listesine Dön
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>