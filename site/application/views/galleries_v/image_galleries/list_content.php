<section class="main-container">

    <div class="container">
        <div class="row">

            <div class="main col-md-12">

                <h1 class="page-title">Fotoğraf Galerileri</h1>
                <div class="separator-2"></div>
                <div class="row">

                    <?php foreach ($galleries as $gallery) { ?>

                        <div class="col-sm-4">
                            <div class="image-box shadow text-center mb-20">
                                <div class="overlay-container overlay-visible">
                                    <img <?php if (get_gallery_cover_image($gallery->folder_name) != false) { ?>
                                        src="<?php echo base_url("panel/uploads/imageGallery_v/$gallery->folder_name/350x216/") . get_gallery_cover_image($gallery->folder_name); ?>"
                                    <?php } else { ?>
                                        src="<?php echo base_url("panel/uploads/default/350x216.png"); ?>"
                                    <?php } ?>
                                            alt="<?php echo $gallery->gallery_name; ?>">
                                    <a href="<?php echo base_url("fotograf-galerisi/$gallery->url"); ?>"
                                       class="overlay-link"><i class="fa fa-link"></i></a>
                                    <div class="overlay-bottom hidden-xs">
                                        <div class="text">
                                            <p class="lead margin-clear"><?php echo $gallery->gallery_name; ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php } ?>
                </div>

            </div>

        </div>
    </div>

</section>