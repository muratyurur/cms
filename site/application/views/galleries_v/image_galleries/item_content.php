<section class="main-container">
    <div class="container">
        <div class="row">
            <div class="main col-md-12">
                <h1 class="page-title"><?php echo $gallery->gallery_name; ?></h1>
                <div class="separator-2"></div>
                <div class="row grid-space-20">
                    <?php if (!empty($images)) { $index = 1;?>
                        <?php foreach ($images as $image) { ?>
                            <div class="col-3 mb-20">
                                <div class="overlay-container">
                                    <img src="<?php echo base_url("panel/uploads/imageGallery_v/$gallery->folder_name/253x156/$image->image_url"); ?>">
                                    <a href="<?php echo base_url("panel/uploads/imageGallery_v/$gallery->folder_name/1140x705/$image->image_url"); ?>"
                                       class="overlay-link small popup-img"
                                       title="<?php echo $gallery->gallery_name . " | Fotoğraf " . $index++; ?>">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } else { ?>
                        <div class="alert alert-warning col-md-12 text-center">
                            <div style="font-size: 18px">
                                <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                Bu galeriye ait herhangi bir içerik bulunamadı...
                            </div>
                        </div>
                    <?php } ?>
                    <div class="col-md-12">
                        <a href="<?php echo base_url("fotograf-galerisi"); ?>" class="btn btn-default">
                            <i class="fa fa-chevron-left"></i>
                            Galeri Listesine Dön
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>