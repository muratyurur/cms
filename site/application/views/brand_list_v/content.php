<section class="main-container">
    <div class="container">
        <div class="row">
            <div class="main col-12">
                <h1 class="page-title">Çalıştığımız Markalar</h1>
                <div class="separator-2"></div>
                <p class="lead">Aşağıda hizmetlerimizde kullanmış olduğumuz markalardan bazılarını bulabilirsiniz...</p>
                <div class="row">
                <?php foreach ($brands as $brand) { ?>
                    <div class="col-sm-4">
                        <div class="image-box shadow text-center mb-20">
                            <div class="overlay-container">
                                <img src="<?php echo base_url("panel/uploads/brand_v/350x208/$brand->img_url"); ?>" alt="">
                                <div class="overlay-top">
                                    <div class="links">
                                    </div>
                                </div>
                                <div class="overlay-bottom">
                                    <div class="links">
                                        <h3>
                                            <a href="<?php echo base_url("panel/uploads/brand_v/1140x705/$brand->img_url"); ?>">
                                                <?php echo $brand->title; ?>
                                            </a>
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>
