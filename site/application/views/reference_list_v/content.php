<section class="main-container">
    <div class="container">
        <h1 class="page-title">Referans Listesi</h1>
        <p>Referanslarımızdan bazıları...</p>
        <div class="separator-2"></div>
        <div class="row">

            <?php $index = 0; ?>
            <?php foreach ($references as $reference) { ?>
                <div class="image-box style-3 mb-20 shadow bordered light-gray-bg">
                    <div class="row grid-space-0">

                        <?php if (($index % 2) == 0) { ?>

                            <div class="col-lg-6">
                                <div class="overlay-container">
                                    <img src="<?php echo base_url("panel/uploads/reference_v/560x352/$reference->img_url"); ?>"
                                         alt="<?php echo $reference->title; ?>">
                                    <div class="overlay-to-top">
                                        <p class="small margin-clear"><em><?php echo $reference->title; ?></em></p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="body">
                                    <div class="pv-30 hidden-lg-down"></div>
                                    <h3><?php echo $reference->title; ?></h3>
                                    <div class="separator-2"></div>
                                    <p class="margin-clear"><?php echo character_limiter(strip_tags($reference->description), 250); ?></p>
                                    <br>
                                    <a href="<?php echo base_url("referans-detay/$reference->url"); ?>"
                                       class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">İncele<i class="fa fa-arrow-right pl-10"></i></a>
                                </div>
                            </div>

                        <?php } else { ?>

                            <div class="col-lg-6">
                                <div class="body">
                                    <div class="pv-30 hidden-lg-down"></div>
                                    <h3><?php echo $reference->title; ?></h3>
                                    <div class="separator-2"></div>
                                    <p class="margin-clear"><?php echo character_limiter(strip_tags($reference->description), 250); ?></p>
                                    <br>
                                    <a href="<?php echo base_url("referans-detay/$reference->url"); ?>"
                                       class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear">İncele<i class="fa fa-arrow-right pl-10"></i></a>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="overlay-container">
                                    <img src="<?php echo base_url("panel/uploads/reference_v/560x352/$reference->img_url"); ?>"
                                         alt="<?php echo $reference->title; ?>">
                                    <div class="overlay-to-top">
                                        <p class="small margin-clear"><em><?php echo $reference->title; ?></em></p>
                                    </div>
                                </div>
                            </div>

                        <?php } ?>

                        <?php $index++; ?>

                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</section>