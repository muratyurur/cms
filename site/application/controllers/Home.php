<?php

class Home extends CI_Controller
{

    public $viewFolder = "";

    public function __construct()
    {
        parent::__construct();

        $this->viewFolder = "homepage";
        $this->load->helper("text");

    }

    public function index()
    {
        // AnaSayfa
        $viewData = new stdClass();

        $this->load->model("slide_model");

        $slides = $this->slide_model->get_all(
            array(
                "isActive" => 1
            ), "rank ASC"
        );

        $this->load->model("service_model");

        $services = $this->service_model->get_limited(
            array(
                "isActive" => 1
            ), 3, "rand()"
        );

        $this->load->model("portfolio_model");

        $portfolios = $this->portfolio_model->get_limited(
            array(
                "isActive" => 1
            ), 4, "rand()"
        );

        $this->load->model("settings_model");

        $settings = $this->settings_model->get();

        $this->load->model("image_model");

        $images = $this->image_model->get_all(
            array(
                "isActive" => 1,
                "gallery_id" => $settings->homepage_gallery
            ), "rank ASC"
        );

        $this->load->model("imageGallery_model");

        $imageGallery = $this->imageGallery_model->get(
            array(
                "id"    => $settings->homepage_gallery
            )
        );

        $this->load->model('testimonial_model');

        $testimonials = $this->testimonial_model->get_all(
            array(
                "isActive" => 1,
            ), "rank ASC"
        );

        $viewData->testimonials = $testimonials;
        $viewData->imageGallery = $imageGallery;
        $viewData->images = $images;
        $viewData->settings = $settings;
        $viewData->slides = $slides;
        $viewData->services = $services;
        $viewData->portfolios = $portfolios;
        $viewData->viewFolder = "home_v";

        $this->load->view($viewData->viewFolder, $viewData);
    }

    public function product_list()
    {
        $viewData = new stdClass();
        $viewData->viewFolder = "product_list_v";

        $this->load->model("product_model");

        $viewData->products = $this->product_model->get_all(
            array(
                "isActive" => 1
            ), "rank ASC"
        );

        $this->load->view($viewData->viewFolder, $viewData);

    }

    public function product_detail($url = "")
    {

        $viewData = new stdClass();
        $viewData->viewFolder = "product_v";

        $this->load->model("product_model");
        $this->load->model("product_image_model");

        $viewData->product = $this->product_model->get(
            array(
                "isActive" => 1,
                "url" => $url
            ), "rank ASC"
        );

        $viewData->product_images = $this->product_image_model->get_all(
            array(
                "isActive" => 1,
                "product_id" => $viewData->product->id
            )
        );

        $viewData->other_products = $this->product_model->get_limited(
            array(
                "isActive" => 1,
            ), 3, "rand()"
        );

        $this->load->view($viewData->viewFolder, $viewData);

    }

    public function portfolio_list()
    {

        $viewData = new stdClass();
        $viewData->viewFolder = "portfolio_list_v";

        $this->load->model("portfolio_model");
        $this->load->model("portfolio_category_model");

        $viewData->portfolios = $this->portfolio_model->get_all(
            array(
                "isActive" => 1
            ), "rank ASC"
        );

        $this->load->view($viewData->viewFolder, $viewData);

    }

    public function portfolio_detail($url = "")
    {

        $viewData = new stdClass();
        $viewData->viewFolder = "portfolio_v";

        $this->load->model("portfolio_model");
        $this->load->model("portfolio_image_model");

        $viewData->portfolio = $this->portfolio_model->get(
            array(
                "isActive" => 1,
                "url" => $url
            ), "rank ASC"
        );

        $viewData->portfolio_images = $this->portfolio_image_model->get_all(
            array(
                "isActive" => 1,
                "portfolio_id" => $viewData->portfolio->id
            )
        );

        $viewData->other_portfolios = $this->portfolio_model->get_limited(
            array(
                "isActive" => 1,
            ), 3, "rand()"
        );

        $this->load->view($viewData->viewFolder, $viewData);

    }

    public function course_list()
    {

        $viewData = new stdClass();
        $viewData->viewFolder = "course_list_v";

        $this->load->model("course_model");

        $viewData->courses = $this->course_model->get_all(
            array(
                "isActive" => 1
            ), "rank ASC"
        );

        $this->load->view($viewData->viewFolder, $viewData);

    }

    public function course_detail($url = "")
    {

        $viewData = new stdClass();
        $viewData->viewFolder = "course_v";

        $this->load->model("course_model");

        $viewData->course = $this->course_model->get(
            array(
                "isActive" => 1,
                "url" => $url
            ), "rank ASC"
        );

        $viewData->other_courses = $this->course_model->get_limited(
            array(
                "isActive" => 1,
            ), 3, "rand()"
        );

        $this->load->view($viewData->viewFolder, $viewData);

    }

    public function reference_list()
    {

        $viewData = new stdClass();
        $viewData->viewFolder = "reference_list_v";

        $this->load->model("reference_model");

        $viewData->references = $this->reference_model->get_all(
            array(
                "isActive" => 1
            ), "rank ASC"
        );

        $this->load->view($viewData->viewFolder, $viewData);

    }

    public function reference_detail($url = "")
    {

        $viewData = new stdClass();
        $viewData->viewFolder = "reference_v";

        $this->load->model("reference_model");

        $viewData->reference = $this->reference_model->get(
            array(
                "isActive" => 1,
                "url" => $url
            ), "rank ASC"
        );

        $viewData->other_references = $this->reference_model->get_limited(
            array(
                "isActive" => 1,
            ), 3, "rand()"
        );

        $this->load->view($viewData->viewFolder, $viewData);

    }

    public function service_list()
    {

        $viewData = new stdClass();
        $viewData->viewFolder = "service_list_v";

        $this->load->model("service_model");

        $viewData->services = $this->service_model->get_all(
            array(
                "isActive" => 1
            ), "rank ASC"
        );

        $this->load->view($viewData->viewFolder, $viewData);

    }

    public function service_detail($url = "")
    {

        $viewData = new stdClass();
        $viewData->viewFolder = "service_v";

        $this->load->model("service_model");

        $viewData->service = $this->service_model->get(
            array(
                "isActive" => 1,
                "url" => $url
            ), "rank ASC"
        );

        $viewData->other_services = $this->service_model->get_limited(
            array(
                "isActive" => 1,
            ), 3, "rand()"
        );

        $this->load->view($viewData->viewFolder, $viewData);

    }

    public function brand_list()
    {

        $viewData = new stdClass();
        $viewData->viewFolder = "brand_list_v";

        $this->load->model("brand_model");

        $viewData->brands = $this->brand_model->get_all(
            array(
                "isActive" => 1
            ), "rank ASC"
        );

        $this->load->view($viewData->viewFolder, $viewData);

    }

    public function about_us()
    {

        $viewData = new stdClass();
        $viewData->viewFolder = "about_v";

        $this->load->model("settings_model");

        $viewData->settings = $this->settings_model->get();

        $this->load->view($viewData->viewFolder, $viewData);

    }

    public function contact()
    {

        $viewData = new stdClass();
        $viewData->viewFolder = "contact_v";

        $this->load->helper("captcha");

        $config = array(
            "word" => '',
            "img_path" => 'captcha/',
            "img_url" => base_url("captcha"),
            "font_path" => 'fonts/corbel.ttf',
            "img_width" => 150,
            "img_height" => 50,
            "expiration" => 7200,
            "word_length" => 5,
            "font_size" => 20,
            "img_id" => "captcha_img",
            "pool" => "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ",
            "colors" => array(
                'background' => array(255, 255, 153),
                'border' => array(255, 255, 255),
                'text' => array(0, 51, 102),
                'grid' => array(102, 178, 255)
            )

        );

        $viewData->captcha = create_captcha($config);

        $this->session->set_userdata("captcha", $viewData->captcha["word"]);

        $this->load->view($viewData->viewFolder, $viewData);

    }

    public function send_contact_message()
    {

        $this->load->library("form_validation");

        $this->form_validation->set_rules("name", "İsim", "trim|required");
        $this->form_validation->set_rules("email", "ePosta", "trim|required|valid_email");
        $this->form_validation->set_rules("subject", "Konu Başlığı", "trim|required");
        $this->form_validation->set_rules("message", "Mesaj", "trim|required");
        $this->form_validation->set_rules("captcha", "Doğrulama Kodu", "trim|required");

        if ($this->form_validation->run() === false) {
            // TODO alert işlemi gerçekleştirilecek.
            redirect(base_url("iletisim"));
        } else {
            if ($this->session->userdata("captcha") == $this->input->post("captcha")) {
                $name = $this->input->post("name");
                $email = $this->input->post("email");
                $subject = $this->input->post("subject");
                $message = $this->input->post("message");

                $email_message = "{$name} isimli ziyaretçi mesaj bıraktı! <br><b>Mesajı: </b> {$message} <br> <b>ePosta: </b> {$email}";

                if (send_mail("", "Site İletişim Formu | $subject", $email_message)) {
                    // TODO alert işlemi gerçekleştirilecek.

                } else {

                    // TODO alert işlemi gerçekleştirilecek.
                }
            } else {
                // TODO alert işlemi gerçekleştirilecek.
                redirect(base_url("iletisim"));
            }
        }

    }

    public function make_me_member()
    {
        $this->load->library("form_validation");

        $this->form_validation->set_rules("subscribe2", "ePosta", "trim|required|valid_email");

        if ($this->form_validation->run() === false) // TODO alert işlemi gerçekleştirilecek.

        {
            // TODO alert işlemi gerçekleştirilecek.

            redirect(base_url("iletisim"));

        } else {

            $this->load->model("member_model");

            $insert = $this->member_model->add(
                array(
                    "email" => $this->input->post("subscribe2"),
                    "ip_address" => $this->input->ip_address(),
                    "isActive" => 1,
                    "createdAt" => date("Y-m-d H:i:s")
                )
            );

            if ($insert) {

                // TODO alert işlemi gerçekleştirilecek.

            } else {

                // TODO alert işlemi gerçekleştirilecek.

            }

        }

        redirect(base_url("iletisim"));

    }

    public function news_list()
    {

        $viewData = new stdClass();
        $viewData->viewFolder = "news_list_v";

        $this->load->model("news_model");

        $viewData->news_list = $this->news_model->get_all(
            array(
                "isActive" => 1
            ), "rank ASC"
        );

        $this->load->view($viewData->viewFolder, $viewData);

    }

    public function news($url = "")
    {

        if ($url != "") {

            $viewData = new stdClass();
            $viewData->viewFolder = "news_v";

            $this->load->model("news_model");

            $news = $this->news_model->get(
                array(
                    "isActive" => 1,
                    "url" => $url
                ), "rank ASC"
            );

            if ($news) {

                $viewData->other_news = $this->news_model->get_limited(
                    array(
                        "isActive" => 1,
                        "id !=" => $news->id
                    ), 4, "rand()"
                );

                $viewCount = $news->viewCount + 1;

                $this->news_model->update(
                    array(
                        "id" => $news->id
                    ),
                    array(
                        "viewCount" => $viewCount
                    )
                );

                $viewData->news = $news;

                $viewData->opengraph = true;

                $this->load->view($viewData->viewFolder, $viewData);

            } else {

                // TODO Alert işlemi gerçekleştirilecek...

            }

        } else {

            // TODO Alert işlemi gerçekleştirilecek...

        }

    }

    public function popup_never_show_again()
    {
        $popup_id = $this->input->post("popup_id");

        set_cookie($popup_id, "true", 60 * 60 * 24 * 365);
    }

    /********** Galeriler için kullanılan metodlar ***********/

    public function image_gallery_list()
    {
        $viewData = new stdClass();
        $viewData->viewFolder = "galleries_v";
        $viewData->subViewFolder = "image_galleries";
        $viewData->viewName = "list_content";

        $this->load->model("imageGallery_model");

        $viewData->galleries = $this->imageGallery_model->get_all(
            array(
                "isActive" => 1,
            )
        );

        $this->load->view($viewData->viewFolder, $viewData);
    }

    public function image_gallery($gallery_url = "")
    {
        if ($gallery_url) {
            $viewData = new stdClass();
            $viewData->viewFolder = "galleries_v";
            $viewData->subViewFolder = "image_galleries";
            $viewData->viewName = "item_content";
            $viewData->gallery = get_image_gallery_by_url($gallery_url);

            $this->load->model("image_model");

            $viewData->images = $this->image_model->get_all(
                array(
                    "isActive" => 1,
                    "gallery_id" => $viewData->gallery->id
                )
            );

            $this->load->view($viewData->viewFolder, $viewData);
        }
    }

    public function video_gallery_list()
    {
        $viewData = new stdClass();
        $viewData->viewFolder = "galleries_v";
        $viewData->subViewFolder = "video_galleries";
        $viewData->viewName = "list_content";

        $this->load->model("videoGallery_model");
        $this->load->model("video_model");

        $viewData->galleries = $this->videoGallery_model->get_all(
            array(
                "isActive" => 1,
            )
        );

        $this->load->view($viewData->viewFolder, $viewData);
    }

    public function video_gallery($gallery_url = "")
    {
        if ($gallery_url) {
            $viewData = new stdClass();
            $viewData->viewFolder = "galleries_v";
            $viewData->subViewFolder = "video_galleries";
            $viewData->viewName = "item_content";
            $viewData->gallery = get_video_gallery_by_url($gallery_url);

            $this->load->model("video_model");

            $viewData->videos = $this->video_model->get_all(
                array(
                    "isActive" => 1,
                    "gallery_id" => $viewData->gallery->id
                )
            );

            $this->load->view($viewData->viewFolder, $viewData);
        }
    }

    public function file_gallery_list()
    {
        $viewData = new stdClass();
        $viewData->viewFolder = "galleries_v";
        $viewData->subViewFolder = "file_galleries";
        $viewData->viewName = "list_content";

        $this->load->model("fileGallery_model");

        $viewData->galleries = $this->fileGallery_model->get_all(
            array(
                "isActive" => 1,
            )
        );

        $this->load->view($viewData->viewFolder, $viewData);
    }

    public function file_gallery($gallery_url = "")
    {
        if ($gallery_url) {
            $viewData = new stdClass();
            $viewData->viewFolder = "galleries_v";
            $viewData->subViewFolder = "file_galleries";
            $viewData->viewName = "item_content";
            $viewData->gallery = get_file_gallery_by_url($gallery_url);

            $this->load->model("file_model");

            $viewData->files = $this->file_model->get_all(
                array(
                    "isActive" => 1,
                    "gallery_id" => $viewData->gallery->id
                )
            );

            $this->load->view($viewData->viewFolder, $viewData);
        }
    }
}
