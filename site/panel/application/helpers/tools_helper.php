<?php

function convertToSEO($text)
{

    $turkce = array("ç", "Ç", "ğ", "Ğ", "ü", "Ü", "ö", "Ö", "ı", "İ", "ş", "Ş", ".", ",", "!", "'", "\"", " ", "?", "*", "_", "|", "=", "(", ")", "[", "]", "{", "}");
    $convert = array("c", "c", "g", "g", "u", "u", "o", "o", "i", "i", "s", "s", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-");

    return strtolower(str_replace($turkce, $convert, $text));

}

function get_readable_date($date)
{

    return strftime('%d.%m.%Y', strtotime($date));

}

function rrmdir($src)
{
    $dir = opendir($src);
    while (false !== ($file = readdir($dir))) {
        if (($file != '.') && ($file != '..')) {
            $full = $src . '/' . $file;
            if (is_dir($full)) {
                rrmdir($full);
            } else {
                unlink($full);
            }
        }
    }
    closedir($dir);
    rmdir($src);
}

function get_active_user()
{
    $t = &get_instance();

    $user = $t->session->userdata("user");

    if ($user) {
        return $user;
    } else {
        return false;
    }
}

function send_mail($toEmail = "", $subject = "", $message = "")
{
    $t = &get_instance();

    $t->load->model("emailSettings_model");

    $email_settings = $t->emailSettings_model->get(
        array(
            "id" => 2
        )
    );

    $config = array(
        "protocol" => $email_settings->protocol,
        "smtp_host" => $email_settings->host,
        "smtp_port" => $email_settings->port,
        "smtp_user" => $email_settings->user,
        "smtp_pass" => $email_settings->password,
        "starttls" => true,
        "charset" => "utf-8",
        "mailtype" => "html",
        "wordwrap" => true,
        "newline" => "\r\n",
    );

    $t->load->library("email", $config);

    $t->email->from($email_settings->user, $email_settings->user_name);
    $t->email->to($toEmail);
    $t->email->subject($subject);
    $t->email->message($message);

    return $t->email->send();
}

function get_settings()
{
    $t = &get_instance();

    $t->load->model("settings_model");

    if ($t->session->userdata("settings")) {
        $settings = $t->session->userdata("settings");
    } else {
        $settings = $t->settings_model->get();

        if (!$settings) {
            $settings = new stdClass();

            $settings->company_name = "Yönetim Paneli";
            $settings->logo = "default";
        }

    }

    return $settings;
}

function get_category_title($category_id = 0)
{
    $t = &get_instance();

    $t->load->model("portfolioCategory_model");

    $category = $t->portfolioCategory_model->get(
        array(
            "id" => $category_id
        )
    );

    if ($category)
        return $category->title;
    else
        return "Tanımlı Değil";
}

function get_user_role_title($user_role_id = 0)
{
    $t = &get_instance();

    $t->load->model("userRole_model");

    $user_role = $t->userRole_model->get(
        array(
            "id" => $user_role_id
        )
    );

    if ($user_role)
        return $user_role->title;
    else
        return "Tanımlı Değil";
}

function get_gallery_name($gallery_id = 0)
{
    $t = &get_instance();

    $t->load->model("imageGallery_model");

    $gallery = $t->imageGallery_model->get(
        array(
            "id" => $gallery_id
        )
    );

    if ($gallery)
        return $gallery->title;
    else
        return "Tanımlı Değil";
}

function upload_picture($file, $uploadPath, $width, $height, $name)
{
    $t = &get_instance();

    $t->load->library("simpleimagelib");

    if (!is_dir("{$uploadPath}/{$width}x{$height}")) {
        mkdir("{$uploadPath}/{$width}x{$height}");
    }

    $upload_error = false;

    try {
        $simpleImage = $t->simpleimagelib->get_simple_image_instance();

        $simpleImage
            ->fromFile($file)
            ->thumbnail($width, $height, 'center')
            ->toFile("{$uploadPath}/{$width}x{$height}/$name", null, 75);

    } catch (Exception $err) {
        $error = $err->getMessage();
        $upload_error = true;
    }

    if ($upload_error) {
        echo $error;
    } else {
        return true;
    }
}

function get_picture($path = "", $picture = "", $resolution = "50x50")
{
    if ($picture != "") {
        if (file_exists(FCPATH . "uploads/$path/$resolution/$picture")) {
            $picture = base_url("uploads/$path/$resolution/$picture");
        } else {
            $picture = base_url("assets/assets/images/default_image.png");
        }
    } else {
        $picture = base_url("assets/assets/images/default_image.png");
    }

    return $picture;
}

function get_page_list($page)
{
    $page_list = array(
        "home_v" => "Ana Sayfa",
        "course_list_v" => "Eğitimler Sayfası",
        "galleries" => "Galeri Sayfaları",
        "news_list_v" => "Haberler Sayfası",
        "about_v" => "Hakkımızda Sayfası",
        "service_list_v" => "Hizmetler Sayfası",
        "contact_v" => "İletişim Sayfası",
        "brand_list_v" => "Markalar Sayfası",
        "portfolio_list_v" => "Portfolyo Sayfası",
        "reference_list_v" => "Referanslar Sayfası",
        "product_list_v" => "Ürünler Sayfası",
    );

    return (empty($page)) ? $page_list : $page_list[$page];
}

function isAdmin()
{
    $t = &get_instance();

    $user = $t->session->userdata("user");

//    return true;

    if($user->user_role_id == 1)
        return true;
    else
        return false;
}

function get_controller_list()
{
    $t = &get_instance();

    $controllers = array();
    $t->load->helper("file");

    $files = get_dir_file_info(APPPATH. "controllers", FALSE);

    foreach (array_keys($files) as $file){
        if($file !== "index.html")
        {
            $controllers[] = strtolower(str_replace(".php", '', $file));
        }
    }

    asort($controllers);

    return $controllers;
}

function getUserRoles()
{
    $t = &get_instance();
    return $t->session->userdata("user_roles");
}

function setUserRoles()
{
    $t = &get_instance();

    $t->load->model("userRole_model");

    $user_roles = $t->userRole_model->get_all(
        array(
            "isActive"  => 1
        )
    );

    $roles = [];
    foreach ($user_roles as $role){
        $roles[$role->id] = $role->permissions;
    }
    $t->session->set_userdata("user_roles", $roles);
}

function isAllowedViewModule($moduleName = "")
{

    $t = &get_instance();
    $moduleName = ($moduleName == "") ? $t->router->fetch_class() : strtolower($moduleName);

    $user = get_active_user();
    $user_roles = getUserRoles();

    if (isset($user_roles[$user->user_role_id])) {
        $permission = json_decode($user_roles[$user->user_role_id]);
        if (isset($permission->$moduleName) && isset($permission->$moduleName->read)) {
            return true;
        }
    }

    return false;
}

function isAllowedWriteModule($moduleName = "")
{

    $t = &get_instance();
    $moduleName = ($moduleName == "") ? $t->router->fetch_class() : strtolower($moduleName);

    $user = get_active_user();
    $user_roles = getUserRoles();

    if (isset($user_roles[$user->user_role_id])) {
        $permission = json_decode($user_roles[$user->user_role_id]);
        if (isset($permission->$moduleName) && isset($permission->$moduleName->write)) {
            return true;
        }
    }

    return false;
}

function isAllowedUpdateModule($moduleName = "")
{

    $t = &get_instance();
    $moduleName = ($moduleName == "") ? $t->router->fetch_class() : strtolower($moduleName);

    $user = get_active_user();
    $user_roles = getUserRoles();

    if (isset($user_roles[$user->user_role_id])) {
        $permission = json_decode($user_roles[$user->user_role_id]);
        if (isset($permission->$moduleName) && isset($permission->$moduleName->update)) {
            return true;
        }
    }

    return false;
}

function isAllowedDeleteModule($moduleName = "")
{

    $t = &get_instance();
    $moduleName = ($moduleName == "") ? $t->router->fetch_class() : strtolower($moduleName);

    $user = get_active_user();
    $user_roles = getUserRoles();

    if (isset($user_roles[$user->user_role_id])) {
        $permission = json_decode($user_roles[$user->user_role_id]);
        if (isset($permission->$moduleName) && isset($permission->$moduleName->delete)) {
            return true;
        }
    }

    return false;
}

?>