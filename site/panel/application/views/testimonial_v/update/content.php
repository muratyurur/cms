<div class="row">
    <div class="col-md-12">
        <h4 class="m-b-lg">
            <?php echo "<b>$item->title</b> kaydını düzenliyorsunuz"; ?>
        </h4>
    </div><!-- END column -->
    <div class="col-md-12">
        <div class="widget">
            <div class="widget-body">
                <form action="<?php echo base_url("testimonials/update/$item->id"); ?>" method="post"
                      enctype="multipart/form-data">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label>Ad Soyad</label>
                            <input class="form-control" placeholder="Ad Soyad" name="full_name"
                                   value="<?php echo isset($form_error) ? set_value("full_name") : $item->full_name; ?>">
                            <?php if (isset($form_error)) { ?>
                                <small class="pull-right input-form-error"> <?php echo form_error("full_name"); ?></small>
                            <?php } ?>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Şirket Adı</label>
                            <input class="form-control" placeholder="Şirket Adı" name="company"
                                   value="<?php echo isset($form_error) ? set_value("company") : $item->company; ?>">
                            <?php if (isset($form_error)) { ?>
                                <small class="pull-right input-form-error"> <?php echo form_error("company"); ?></small>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Başlık</label>
                        <input class="form-control" placeholder="Başlık" name="title"
                               value="<?php echo isset($form_error) ? set_value("title") : $item->title; ?>">
                        <?php if (isset($form_error)) { ?>
                            <small class="pull-right input-form-error"> <?php echo form_error("title"); ?></small>
                        <?php } ?>
                    </div>

                    <div class="form-group">
                        <label>Mesaj Metni</label>
                        <textarea class="form-control" name="description" cols="30" rows="10"><?php echo $item->description; ?></textarea>
                        <?php if (isset($form_error)) { ?>
                            <small class="pull-right input-form-error"> <?php echo form_error("description"); ?></small>
                        <?php } ?>
                    </div>

                    <div class="row">

                        <div class="col-md-1 image_upload_container">
                            <img src="<?php echo get_picture($viewFolder, $item->img_url, "90x90"); ?>"
                                 alt=""
                                 class="img-responsive img-rounded" style="margin: 0 auto">
                        </div>

                        <div class="col-md-11 form-group image_upload_container">
                            <label>Görsel Seçiniz</label>
                            <input type="file" name="img_url" class="form-control">
                        </div>

                    </div>
                    <div class="row" style="padding: 12px;">
                        <button type="submit" class="btn btn-primary btn-md btn-outline">
                            <i class="fa fa-refresh"></i>
                            Güncelle
                        </button>
                        <a href="<?php echo base_url("testimonials"); ?>" class="btn btn-md btn-danger btn-outline">
                            <i class="fa fa-times"></i>
                            İptal</a>
                    </div>
                </form>
            </div><!-- .widget-body -->
        </div><!-- .widget -->
    </div><!-- END column -->
</div>