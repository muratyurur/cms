<div class="row">
    <div class="col-md-12">
        <h4 class="m-b-lg">
            Yeni Ziyaretçi Notu Ekle
        </h4>
    </div><!-- END column -->
    <div class="col-md-12">
        <div class="widget">
            <div class="widget-body">
                <form action="<?php echo base_url("testimonials/save"); ?>" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label>Ad Soyad</label>
                            <input class="form-control" placeholder="Ad Soyad" name="full_name"
                                   value="<?php echo isset($form_error) ? set_value("full_name") : ""; ?>">
                            <?php if (isset($form_error)) { ?>
                                <small class="pull-right input-form-error"> <?php echo form_error("full_name"); ?></small>
                            <?php } ?>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Şirket Adı</label>
                            <input class="form-control" placeholder="Şirket Adı" name="company"
                                   value="<?php echo isset($form_error) ? set_value("company") : ""; ?>">
                            <?php if (isset($form_error)) { ?>
                                <small class="pull-right input-form-error"> <?php echo form_error("company"); ?></small>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Başlık</label>
                        <input class="form-control" placeholder="Başlık" name="title"
                               value="<?php echo isset($form_error) ? set_value("title") : ""; ?>">
                        <?php if (isset($form_error)) { ?>
                            <small class="pull-right input-form-error"> <?php echo form_error("title"); ?></small>
                        <?php } ?>
                    </div>

                    <div class="form-group">
                        <label>Mesaj Metni</label>
                        <textarea class="form-control" name="description" cols="30" rows="10"></textarea>
                        <?php if (isset($form_error)) { ?>
                            <small class="pull-right input-form-error"> <?php echo form_error("description"); ?></small>
                        <?php } ?>
                    </div>

                    <div class="form-group image_upload_container">
                        <label>Görsel Seçiniz</label>
                        <input type="file" name="img_url" class="form-control"
                               value="<?php echo isset($form_error) ? set_value("img_url") : ""; ?>">
                    </div>

                    <button type="submit" class="btn btn-primary btn-md btn-outline">
                        <i class="fa fa-floppy-o"></i>
                        Kaydet
                    </button>
                    <a href="<?php echo base_url("testimonials"); ?>" class="btn btn-md btn-danger btn-outline">
                        <i class="fa fa-times"></i>
                        İptal
                    </a>
                </form>
            </div><!-- .widget-body -->
        </div><!-- .widget -->
    </div><!-- END column -->
</div>