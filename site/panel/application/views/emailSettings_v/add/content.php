<div class="row">
    <div class="col-md-12">
        <h4 class="m-b-lg">
            Yeni ePosta Ayar Seti Ekle
        </h4>
    </div><!-- END column -->
    <div class="col-md-12">
        <div class="widget">
            <div class="widget-body">
                <form action="<?php echo base_url("emailSettings/save"); ?>" method="post" enctype="multipart/form-data">

                    <div class="form-group">
                        <label>Protokol</label>
                        <input class="form-control" placeholder="Protokol" name="protocol" value="<?php echo isset($form_error) ? set_value("protocol") : ""; ?>">
                        <?php if (isset($form_error)) { ?>
                            <small class="pull-right input-form-error"> <?php echo form_error("protocol"); ?></small>
                        <?php } ?>
                    </div>

                    <div class="form-group">
                        <label>Sunucu Adı</label>
                        <input class="form-control" placeholder="Sunucu Adı" name="host" value="<?php echo isset($form_error) ? set_value("host") : ""; ?>">
                        <?php if (isset($form_error)) { ?>
                            <small class="pull-right input-form-error"> <?php echo form_error("host"); ?></small>
                        <?php } ?>
                    </div>

                    <div class="form-group">
                        <label>Port Numarası</label>
                        <input type="text" class="form-control" placeholder="Port Numarası" name="port" value="<?php echo isset($form_error) ? set_value("port") : ""; ?>">
                        <?php if (isset($form_error)) { ?>
                            <small class="pull-right input-form-error"> <?php echo form_error("port"); ?></small>
                        <?php } ?>
                    </div>

                    <div class="form-group">
                        <label>ePosta Adresi (user)</label>
                        <input type="email" class="form-control" placeholder="ePosta Adresi (user)" name="user" value="<?php echo isset($form_error) ? set_value("user") : ""; ?>">
                        <?php if (isset($form_error)) { ?>
                            <small class="pull-right input-form-error"> <?php echo form_error("user"); ?></small>
                        <?php } ?>
                    </div>

                    <div class="form-group">
                        <label>ePosta Şifresi</label>
                        <input type="password" class="form-control" placeholder="ePosta Şifresi" name="password">
                        <?php if (isset($form_error)) { ?>
                            <small class="pull-right input-form-error"> <?php echo form_error("password"); ?></small>
                        <?php } ?>
                    </div>
    
                    <div class="form-group">
                        <label>Kimden (from)</label>
                        <input type="email" class="form-control" placeholder="Kimden (from)" name="from" value="<?php echo isset($form_error) ? set_value("from") : ""; ?>">
                        <?php if (isset($form_error)) { ?>
                            <small class="pull-right input-form-error"> <?php echo form_error("from"); ?></small>
                        <?php } ?>
                    </div>
    
                    <div class="form-group">
                        <label>Kime (to)</label>
                        <input type="email" class="form-control" placeholder="Kime (to)" name="to" value="<?php echo isset($form_error) ? set_value("to") : ""; ?>">
                        <?php if (isset($form_error)) { ?>
                            <small class="pull-right input-form-error"> <?php echo form_error("to"); ?></small>
                        <?php } ?>
                    </div>
    
                    <div class="form-group">
                        <label>Gönderici Adı</label>
                        <input type="text" class="form-control" placeholder="Gönderici Adı" name="user_name" value="<?php echo isset($form_error) ? set_value("user_name") : ""; ?>">
                        <?php if (isset($form_error)) { ?>
                            <small class="pull-right input-form-error"> <?php echo form_error("user_name"); ?></small>
                        <?php } ?>
                    </div>

                    <button type="submit" class="btn btn-primary btn-md btn-outline">
                        <i class="fa fa-floppy-o"></i>
                        Kaydet
                    </button>

                    <a href="<?php echo base_url("emailSettings"); ?>" class="btn btn-md btn-danger btn-outline">
                        <i class="fa fa-times"></i>
                        İptal
                    </a>
                </form>
            </div><!-- .widget-body -->
        </div><!-- .widget -->
    </div><!-- END column -->
</div>