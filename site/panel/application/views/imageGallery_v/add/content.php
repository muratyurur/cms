<div class="row">
    <div class="col-md-12">
        <h4 class="m-b-lg">
            Yeni Resim Galerisi Ekle
        </h4>
    </div><!-- END column -->
    <div class="col-md-12">
        <div class="widget">
            <div class="widget-body">
                <form action="<?php echo base_url("imageGallery/save"); ?>" method="post">
                    <div class="form-group">
                        <label>Galeri Adı</label>
                        <input class="form-control" placeholder="Galeri adını giriniz..." name="gallery_name">
                        <?php if(isset($form_error)){ ?>
                            <small class="pull-right input-form-error"> <?php echo form_error("gallery_name"); ?></small>
                        <?php } ?>
                    </div>
                    <button type="submit" class="btn btn-primary btn-md btn-outline">
                        <i class="fa fa-floppy-o"></i>
                        Kaydet</button>
                    <a href="<?php echo base_url("imageGallery"); ?>" class="btn btn-md btn-danger btn-outline">
                        <i class="fa  fa-times-circle"></i>
                        İptal</a>
                </form>
            </div><!-- .widget-body -->
        </div><!-- .widget -->
    </div><!-- END column -->
</div>