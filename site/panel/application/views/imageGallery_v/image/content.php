<div class="row">
    <div class="col-md-12">
        <div class="widget">
            <div class="widget-body">
                <form data-url="<?php echo base_url("imageGallery/refresh_image_list/$item->id/$item->folder_name"); ?>"
                      action="<?php echo base_url("imageGallery/image_upload/$item->id/$item->folder_name"); ?>"
                      id="dropzone"
                      class="dropzone"
                      data-plugin="dropzone"
                      data-options="{ url: '<?php echo base_url("imageGallery/image_upload/$item->id/$item->folder_name"); ?>'}"
                >
                    <div class="dz-message">
                        <h3 class="m-h-lg">Yüklemek istediğiniz resimleri buyara sürükleyiniz</h3>
                        <p class="m-b-lg text-muted">(Yüklemek için dosyalarınızı sürükleyiniz yada buraya tıklayınız)</p>
                    </div>
                </form>
            </div><!-- .widget-body -->
        </div><!-- .widget -->
    </div><!-- END column -->
</div>

<div class="row">
    <div class="col-md-10">
        <h4 class="m-b-lg">
            <b><?php echo $item->gallery_name; ?></b> kaydına ait Resimler
        </h4>
    </div>
    <div class="col-md-2" style="text-align: right">
        <a href="<?php echo base_url("imageGallery") ?>">
            <button class="btn btn-sm btn-outline btn-inverse ml-3">
                <i class="fa fa-chevron-circle-left"></i> Geri Dön
            </button>
        </a>
        <a href="<?php echo base_url("imageGallery/imageDeleteAll/$item->id"); ?>">
            <button class="btn btn-sm btn-deepOrange btn-outline">
                <i class="fa fa-trash-o"></i> Tümünü Sil
            </button>
        </a>
    </div>
    <div class="col-md-12">
        <div class="widget">
            <div class="widget-body image_list_container">

                <?php $this->load->view("{$viewFolder}/{$subViewFolder}/render_elements/image_list_v"); ?>

            </div><!-- .widget-body -->
        </div><!-- .widget -->
    </div><!-- END column -->
</div>

