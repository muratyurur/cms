<?php if (empty($item_images)) { ?>

    <div class="alert alert-info text-center">
        <p>Bu galeride herhangi bir resim bulunmamaktadır.</a></p>
    </div>

<?php } else { ?>

    <table id="datatable-responsive"
           class="table table-hover table-striped table-bordered content-container"
    >
        <thead>
            <th class="order"><i class="fa fa-reorder"></i></th>
            <th>#id</th>
            <th>Görsel</th>
            <th>Resim Adı</th>
            <th>Durumu</th>
            <th>İşlem</th>
        </thead>
        <tbody class="sortable" data-url="<?php echo base_url("imageGallery/imageRankSetter"); ?>">

        <?php foreach ($item_images as $image) { ?>

            <tr id="ord-<?php echo $image->id; ?>">
                <td class="order"><i class="fa fa-reorder"></i></td>
                <td class="w50 text-center">#<?php echo $image->id; ?></td>
                <td class="w100 text-center">
                    <img width="100" src="<?php echo base_url("uploads/{$viewFolder}/$item->folder_name/253x156/$image->image_url"); ?>"
                         alt="<?php echo $image->image_url; ?>" class="img-responsive img-rounded" style="margin: 0 auto">
                </td>
                <td><?php echo $image->image_url; ?></td>
                <td class="w100 text-center">
                    <input
                            data-url="<?php echo base_url("imageGallery/imageIsActiveSetter/$image->id"); ?>"
                            class="isActive"
                            type="checkbox"
                            data-switchery
                            data-color="#188ae2"
                        <?php echo ($image->isActive) ? "checked" : ""; ?>
                    />
                </td>
                <td class="w100 text-center">
                    <button
                            data-url="<?php echo base_url("imageGallery/imageDelete/$image->id/$image->gallery_id"); ?>"
                            class="btn btn-sm btn-danger btn-outline remove-btn btn-block">
                        <i class="fa fa-trash"></i> Sil
                    </button>
                </td>
            </tr>

        <?php } ?>

        </tbody>

    </table>
<?php } ?>