<div class="wrap p-t-0">
    <footer class="app-footer">
        <div class="clearfix">
            <span class="footer-msg">Developed with
                <span class="fa fa-heart" style="color: #c10000">
                    <span class="u-VisuallyHidden">love</span>
                </span>
                <span class="footer-msg"> using &nbsp;</span>
                <a href="https://codeigniter.com" target="_blank">
                    <span style="
                    font-family: Consolas, Monaco, Courier New, Courier, monospace;
                    font-weight: bold;
                    color: #EE4323;
                    vertical-align: central;
                    font-size: 13px">
                    <i class="fa fa-free-code-camp" aria-hidden="true"></i> CodeIgniter
                    </span>
                </a>
                <span class="footer-msg">
                    by&nbsp;
                </span>
                    <a href="https://muratyurur.com" target="_blank">
                        <span class="signature">
                            M.Yürür
                        </span>
                    </a>
            </span>
        </div>
    </footer>
</div>
