<?php $user = get_active_user(); ?>
<?php $settings = get_settings(); ?>

<aside id="menubar" class="menubar light">
    <div class="app-user">
        <div class="media">
            <div class="media-left">
                <div class="avatar avatar-md avatar-circle">
                    <a href="<?php echo base_url("users/update/$user->id"); ?>">
                        <img class="img-responsive" src="<?php echo base_url("/uploads/users_v/$user->img_url"); ?>"
                             alt="<?php echo $user->full_name; ?>"/>
                    </a>
                </div><!-- .avatar -->
            </div>
            <div class="media-body">
                <div class="foldable">
                    <h5><a href="<?php echo base_url("users/update/$user->id"); ?>"
                           class="username"><?php echo $user->full_name; ?></a></h5>
                    <ul>
                        <li class="dropdown">
                            <a href="javascript:void(0)" class="dropdown-toggle usertitle" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">
                                <small>İşlemler</small>
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu animated flipInY">
                                <li>
                                    <a class="text-color" href="<?php echo base_url("users/update_form/$user->id"); ?>">
                                        <span class="m-r-xs"><i class="fa fa-user"></i></span>
                                        <span>Kullanıcı Profili</span>
                                    </a>
                                </li>
                                <li>
                                    <a class="text-color"
                                       href="<?php echo base_url("users/update_password_form/$user->id"); ?>">
                                        <span class="m-r-xs"><i class="fa fa-key"></i></span>
                                        <span>Şifremi Değiştir</span>
                                    </a>
                                </li>
                                <li role="separator" class="divider"></li>
                                <li>
                                    <a class="text-color" href="<?php echo base_url("logout"); ?>">
                                        <span class="m-r-xs"><i class="fa fa-power-off"></i></span>
                                        <span>Güvenli Çıkış</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div><!-- .media-body -->
        </div><!-- .media -->
    </div><!-- .app-user -->

    <div class="menubar-scroll">
        <div class="menubar-scroll-inner">
            <ul class="app-menu">

                <?php if (isAllowedViewModule("dashboard")) { ?>
                    <li>
                        <a href="<?php echo base_url("dashboard"); ?>">
                            <i class="menu-icon zmdi zmdi-view-dashboard zmdi-hc-lg"></i>
                            <span class="menu-text">Kontrol Paneli</span>
                        </a>
                    </li>
                <?php } ?>

                <?php if (isAllowedViewModule("settings")) { ?>
                    <li>
                        <a href="<?php echo base_url("settings"); ?>">
                            <i class="menu-icon zmdi zmdi-settings zmdi-hc-lg"></i>
                            <span class="menu-text">Site Ayarları</span>
                        </a>
                    </li>
                <?php } ?>

                <?php if (isAllowedViewModule("emailSettings")) { ?>
                    <li>
                        <a href="<?php echo base_url("emailSettings"); ?>">
                            <i class="menu-icon zmdi zmdi-email zmdi-hc-lg"></i>
                            <span class="menu-text">ePosta Ayarları</span>
                        </a>
                    </li>
                <?php } ?>

                <?php if (isAllowedViewModule("imageGallery") || isAllowedViewModule("videoGallery") || isAllowedViewModule("fileGallery")) { ?>
                    <li class="has-submenu">
                        <a href="javascript:void(0)" class="submenu-toggle">
                            <i class="menu-icon zmdi zmdi-apps zmdi-hc-lg"></i>
                            <span class="menu-text">Galeriler</span>
                            <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
                        </a>
                        <ul class="submenu">
                            <?php if (isAllowedViewModule("imageGallery")) { ?>
                                <li><a href="<?php echo base_url("imageGallery"); ?>"><span class="menu-text">Resim Galerisi</span></a>
                                </li>
                            <?php } ?>
                            <?php if (isAllowedViewModule("videoGallery")) { ?>
                                <li><a href="<?php echo base_url("videoGallery"); ?>"><span class="menu-text">Video Galerisi</span></a>
                                </li>
                            <?php } ?>
                            <?php if (isAllowedViewModule("fileGallery")) { ?>
                                <li><a href="<?php echo base_url("fileGallery"); ?>"><span class="menu-text">Dosya Galerisi</span></a>
                                </li>
                            <?php } ?>
                        </ul>
                    </li>
                <?php } ?>

                <?php if (isAllowedViewModule("slide")) { ?>
                    <li class="has-submenu">
                        <a href="<?php echo base_url("slide"); ?>">
                            <i class="menu-icon zmdi zmdi-code zmdi-hc-lg"></i>
                            <span class="menu-text">Slider</span>
                        </a>
                    </li>
                <?php } ?>

                <?php if (isAllowedViewModule("product")) { ?>
                    <li class="has-submenu">
                        <a href="<?php echo base_url("product"); ?>">
                            <i class="menu-icon fa fa-cubes"></i>
                            <span class="menu-text">Ürünler</span>
                        </a>
                    </li>
                <?php } ?>

                <?php if (isAllowedViewModule("news")) { ?>
                    <li class="has-submenu">
                        <a href="<?php echo base_url("news"); ?>">
                            <i class="menu-icon fa fa-newspaper-o"></i>
                            <span class="menu-text">Haberler</span>
                        </a>
                    </li>
                <?php } ?>

                <?php if (isAllowedViewModule("service")) { ?>
                    <li class="has-submenu">
                        <a href="<?php echo base_url("service"); ?>">
                            <i class="menu-icon zmdi zmdi-assignment zmdi-hc-lg"></i>
                            <span class="menu-text">Hizmetler</span>
                        </a>
                    </li>
                <?php } ?>

                <?php if (isAllowedViewModule("portfolioCategories") || isAllowedViewModule("portfolio")) { ?>
                    <li class="has-submenu">
                        <a href="javascript:void(0)" class="submenu-toggle">
                            <i class="menu-icon zmdi zmdi-collection-image-o zmdi-hc-lg"></i>
                            <span class="menu-text">Portfolyo İşlemleri</span>
                            <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
                        </a>
                        <ul class="submenu">
                            <?php if (isAllowedViewModule("portfolioCategories")) { ?>
                                <li><a href="<?php echo base_url("portfolioCategories"); ?>"><span class="menu-text">Portfolyo Kategorileri</span></a>
                                </li>
                            <?php } ?>
                            <?php if (isAllowedViewModule("portfolio")) { ?>
                                <li><a href="<?php echo base_url("portfolio"); ?>"><span
                                                class="menu-text">Portfolyo</span></a></li>
                            <?php } ?>
                        </ul>
                    </li>
                <?php } ?>

                <?php if (isAllowedViewModule("course")) { ?>
                    <li class="has-submenu">
                        <a href="<?php echo base_url("course"); ?>">
                            <i class="menu-icon fa fa-calendar"></i>
                            <span class="menu-text">Eğitimler</span>
                        </a>
                    </li>
                <?php } ?>

                <?php if (isAllowedViewModule("reference")) { ?>
                    <li class="has-submenu">
                        <a href="<?php echo base_url("reference"); ?>">
                            <i class="menu-icon zmdi zmdi-check zmdi-hc-lg"></i>
                            <span class="menu-text">Referanslar</span>
                        </a>
                    </li>
                <?php } ?>

                <?php if (isAllowedViewModule("users")) { ?>
                    <li class="has-submenu">
                        <a href="<?php echo base_url("users"); ?>">
                            <i class="menu-icon fa fa-users"></i>
                            <span class="menu-text">Kullanıcılar</span>
                        </a>
                    </li>
                <?php } ?>

                <?php if (isAllowedViewModule("user_roles")) { ?>
                    <li class="has-submenu">
                        <a href="<?php echo base_url("user_roles"); ?>">
                            <i class="menu-icon fa fa-user-secret"></i>
                            <span class="menu-text">Kullanıcı Rolleri</span>
                        </a>
                    </li>
                <?php } ?>

                <?php if (isAllowedViewModule("popup")) { ?>
                    <li class="has-submenu">
                        <a href="<?php echo base_url("popup"); ?>">
                            <i class="menu-icon zmdi zmdi-lamp zmdi-hc-lg"></i>
                            <span class="menu-text">Pop-up Hizmeti</span>
                        </a>
                    </li>
                <?php } ?>

                <?php if (isAllowedViewModule("brand")) { ?>
                    <li class="has-submenu">
                        <a href="<?php echo base_url("brand"); ?>">
                            <i class="menu-icon zmdi zmdi-puzzle-piece zmdi-hc-lg"></i>
                            <span class="menu-text">Markalar</span>
                        </a>
                    </li>
                <?php } ?>

                <?php if (isAllowedViewModule("members")) { ?>
                    <li class="has-submenu">
                        <a href="javascript:void(0)">
                            <i class="menu-icon fa fa-users"></i>
                            <span class="menu-text">Aboneler</span>
                        </a>
                    </li>
                <?php } ?>

                <?php if (isAllowedViewModule("testimonials")) { ?>
                    <li class="has-submenu">
                        <a href="<?php echo base_url("testimonials"); ?>">
                            <i class="menu-icon fa fa-comments"></i>
                            <span class="menu-text">Ziyaretçi Notları</span>
                        </a>
                    </li>
                <?php } ?>

                <li>
                    <a target="_blank" href="https://cms.muratyurur.com">
                        <i class="menu-icon zmdi zmdi-view-web zmdi-hc-lg"></i>
                        <span class="menu-text">Site Ana Sayfa</span>
                    </a>
                </li>
            </ul><!-- .app-menu -->
        </div><!-- .menubar-scroll-inner -->
    </div><!-- .menubar-scroll -->
</aside>