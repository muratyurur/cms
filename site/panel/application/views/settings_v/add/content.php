<div class="row">
    <div class="col-md-12">
        <h4 class="m-b-lg">
            Site Ayarı Ekle
        </h4>
    </div><!-- END column -->
    
    <div class="col-md-12">
        <form action="<?php echo base_url("settings/save"); ?>" method="post" enctype="multipart/form-data">
            
            <div class="widget">
                <div class="m-b-lg nav-tabs-horizontal">
                    <!-- tabs list -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#tab-1" aria-controls="tab-1" role="tab" data-toggle="tab">Site Bilgileri</a></li>
                        <li role="presentation"><a href="#tab-2" aria-controls="tab-2" role="tab" data-toggle="tab">Adres Bilgisi</a></li>
                        <li role="presentation"><a href="#tab-3" aria-controls="tab-3" role="tab" data-toggle="tab">Hakkımızda</a></li>
                        <li role="presentation"><a href="#tab-4" aria-controls="tab-4" role="tab" data-toggle="tab">Misyonumuz</a></li>
                        <li role="presentation"><a href="#tab-5" aria-controls="tab-5" role="tab" data-toggle="tab">Vizyonumuz</a></li>
                        <li role="presentation"><a href="#tab-6" aria-controls="tab-6" role="tab" data-toggle="tab">Sosyal Medya</a></li>
                        <li role="presentation"><a href="#tab-7" aria-controls="tab-7" role="tab" data-toggle="tab">Logo</a></li>
                        <li role="presentation"><a href="#tab-8" aria-controls="tab-7" role="tab" data-toggle="tab">Ana Sayfa Galerisi</a></li>
                    </ul><!-- .nav-tabs -->
                    
                    <!-- Tab panes -->
                    <div class="tab-content p-md">
                        <div role="tabpanel" class="tab-pane in active fade" id="tab-1">
                            
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label>Şirket Adı</label>
                                    <input class="form-control" placeholder="Şirket ya da Sitenizin Adı"
                                           name="company_name"
                                           value="<?php echo isset($form_error) ? set_value("company_name") : ""; ?>">
                                    <?php if (isset($form_error)) { ?>
                                        <small class="pull-right input-form-error"> <?php echo form_error("company_name"); ?></small>
                                    <?php } ?>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Slogan</label>
                                    <input class="form-control" placeholder="Sloganınızı buradan ekleyebilirsiniz"
                                           name="slogan"
                                           value="<?php echo isset($form_error) ? set_value("slogan") : ""; ?>">
                                    <?php if (isset($form_error)) { ?>
                                        <small class="pull-right input-form-error"> <?php echo form_error("slogan"); ?></small>
                                    <?php } ?>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label>Telefon 1</label>
                                    <input class="form-control" placeholder="Telefon numaranız" name="phone_1"
                                           value="<?php echo isset($form_error) ? set_value("phone_1") : ""; ?>">
                                    <?php if (isset($form_error)) { ?>
                                        <small class="pull-right input-form-error"> <?php echo form_error("phone_1"); ?></small>
                                    <?php } ?>
                                </div>
                                
                                <div class="form-group col-md-4">
                                    <label>Telefon 2</label>
                                    <input class="form-control" placeholder="Diğer telefon numaranız (opsiyonel)" name="phone_2">
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label>Faks 1</label>
                                    <input class="form-control" placeholder="Faks numaranız" name="fax_1">
                                </div>
                                
                                <div class="form-group col-md-4">
                                    <label>Faks 2</label>
                                    <input class="form-control" placeholder="Diğer faks numaranız (opsiyonel)" name="fax_2">
                                </div>
                            </div>
                        </div><!-- .tab-pane  -->
                        
                        <div role="tabpanel" class="tab-pane fade" id="tab-2">
                            <div class="row">
                                <div class="form-group col-md-12" style="margin-bottom: 6px!important;">
                                    <label>Adres Bilgisi</label>
                                    <textarea name="address" class="m-0" data-plugin="summernote" data-options="{height: 250}"></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <label>Enlem (Latitude)</label>
                                    <input class="form-control" placeholder="Enlem (Latitude)" name="latitude">
                                </div>

                                <div class="form-group col-md-3">
                                    <label>Boylam (Longtitude)</label>
                                    <input class="form-control" placeholder="Boylam (Longtitude)" name="longtitude">
                                </div>

                                <div class="form-group col-md-6">
                                    <label>Bbox</label>
                                    <input class="form-control" placeholder="Bbox" name="bbox">
                                </div>
                            </div>
                        </div><!-- .tab-pane  -->
                        
                        <div role="tabpanel" class="tab-pane fade" id="tab-3">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Hakkımızda</label>
                                    <textarea name="about_us" class="m-0" data-plugin="summernote" data-options="{height: 250}"></textarea>
                                </div>
                            </div>
                        </div><!-- .tab-pane  -->
                        
                        <div role="tabpanel" class="tab-pane fade" id="tab-4">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Misyonumuz</label>
                                    <textarea name="mission" class="m-0" data-plugin="summernote" data-options="{height: 250}"></textarea>
                                </div>
                            </div>
                        </div><!-- .tab-pane  -->
                        
                        <div role="tabpanel" class="tab-pane fade" id="tab-5">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Vizyonumuz</label>
                                    <textarea name="vision" class="m-0" data-plugin="summernote" data-options="{height: 250}"></textarea>
                                </div>
                            </div>
                        </div><!-- .tab-pane  -->
                        
                        <div role="tabpanel" class="tab-pane fade" id="tab-6">
                            
                            <div class="row">
                                <div class="form-group col-md-8">
                                    <label>ePosta Adresiniz</label>
                                    <input class="form-control" placeholder="ePosta Adresiniz" name="email"
                                           value="<?php echo isset($form_error) ? set_value("email") : ""; ?>">
                                    <?php if (isset($form_error)) { ?>
                                        <small class="pull-right input-form-error"> <?php echo form_error("email"); ?></small>
                                    <?php } ?>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label>Facebook</label>
                                    <input class="form-control" placeholder="Facebook adresiniz" name="facebook"
                                           value="<?php echo isset($form_error) ? set_value("facebook") : ""; ?>">
                                </div>
        
                                <div class="form-group col-md-4">
                                    <label>Twitter</label>
                                    <input class="form-control" placeholder="Twitter Adresiniz" name="twitter"
                                           value="<?php echo isset($form_error) ? set_value("twitter") : ""; ?>">
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label>Instagram</label>
                                    <input class="form-control" placeholder="Instagram adresiniz" name="instagram"
                                           value="<?php echo isset($form_error) ? set_value("instagram") : ""; ?>">
                                </div>
        
                                <div class="form-group col-md-4">
                                    <label>Linkedin</label>
                                    <input class="form-control" placeholder="Linkedin Adresiniz" name="linkedin"
                                           value="<?php echo isset($form_error) ? set_value("linkedin") : ""; ?>">
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label>GitHub</label>
                                    <input class="form-control" placeholder="GitHub adresiniz" name="github">
                                </div>

                                <div class="form-group col-md-4">
                                    <label>GitLab</label>
                                    <input class="form-control" placeholder="GitLab Adresiniz" name="gitlab">
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label>Skype</label>
                                    <input class="form-control" placeholder="Skype adresiniz" name="skype">
                                </div>

                                <div class="form-group col-md-4">
                                    <label>Google Plus</label>
                                    <input class="form-control" placeholder="Google Plus Adresiniz" name="googleplus">
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label>WhatsApp</label>
                                    <input class="form-control" placeholder="WhatsApp adresiniz" name="whatsapp">
                                </div>
                            </div>
                            
                        </div><!-- .tab-pane  -->
                        
                        <div role="tabpanel" class="tab-pane fade" id="tab-7">
    
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label>Masaüstü Logo Seçimi</label>
                                    <input type="file" name="logo" class="form-control">
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label>Mobil Logo Seçimi</label>
                                    <input type="file" name="logo_mobile" class="form-control">
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label>Favicon Seçimi</label>
                                    <input type="file" name="favicon" class="form-control">
                                </div>
                            </div>
                            
                        </div><!-- .tab-pane  -->

                        <div role="tabpanel" class="tab-pane fade" id="tab-8">

                            <div class="row">
                                <div class="form-group col-md-8">
                                    <label>Ana Sayfa da gösterilmesini istediğiniz galeriyi seçiniz...</label>
                                    <select class="form-control" name="homepage_gallery">
                                        <?php foreach ($galleries as $gallery) { ?>
                                            <option value="<?php echo $gallery->id; ?>"><?php echo $gallery->gallery_name; ?></option>
                                        <?php } ?>
                                    </select>
                                    <?php if (isset($form_error)) { ?>
                                        <small class="pull-right input-form-error"> <?php echo form_error("homepage_gallery"); ?></small>
                                    <?php } ?>
                                </div>
                            </div>
                    </div><!-- .tab-pane  -->
                </div><!-- .nav-tabs-horizontal -->
            </div><!-- .widget -->
            <div class="widget">
                <div class="m-b-lg col-md-12">
                    <button type="submit" class="btn btn-primary btn-md">
                        <i class="fa fa-floppy-o"></i>
                        Kaydet
                    </button>
                    
                    <a href="<?php echo base_url("settings"); ?>" class="btn btn-md btn-danger">
                        <i class="fa fa-times"></i>
                        İptal
                    </a>
                </div>
            </div>
        </form>
    </div><!-- END column -->
</div>