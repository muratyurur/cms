<div class="row">
    <div class="col-md-12">
        <h4 class="m-b-lg">
            <?php echo "<b>$item->company_name</b> site ayar setini düzenliyorsunuz"; ?>
        </h4>
    </div><!-- END column -->
    <div class="col-md-12">
        <form action="<?php echo base_url("settings/update/$item->id"); ?>" method="post" enctype="multipart/form-data">
            
            <div class="widget">
                <div class="m-b-lg nav-tabs-horizontal">
                    <!-- tabs list -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#tab-1" aria-controls="tab-1" role="tab" data-toggle="tab">Site Bilgileri</a></li>
                        <li role="presentation"><a href="#tab-2" aria-controls="tab-2" role="tab" data-toggle="tab">Adres Bilgisi</a></li>
                        <li role="presentation"><a href="#tab-3" aria-controls="tab-3" role="tab" data-toggle="tab">Hakkımızda</a></li>
                        <li role="presentation"><a href="#tab-4" aria-controls="tab-4" role="tab" data-toggle="tab">Misyonumuz</a></li>
                        <li role="presentation"><a href="#tab-5" aria-controls="tab-5" role="tab" data-toggle="tab">Vizyonumuz</a></li>
                        <li role="presentation"><a href="#tab-6" aria-controls="tab-6" role="tab" data-toggle="tab">Sosyal Medya</a></li>
                        <li role="presentation"><a href="#tab-7" aria-controls="tab-7" role="tab" data-toggle="tab">Logo</a></li>
                        <li role="presentation"><a href="#tab-8" aria-controls="tab-7" role="tab" data-toggle="tab">Ana Sayfa Galerisi</a></li>
                    </ul><!-- .nav-tabs -->
                    
                    <!-- Tab panes -->
                    <div class="tab-content p-md">
                        <div role="tabpanel" class="tab-pane in active fade" id="tab-1">
                            
                            <div class="row">
                                <div class="form-group col-md-8">
                                    <label>Şirket Adı</label>
                                    <input class="form-control" placeholder="Şirket ya da Sitenizin Adı"
                                           name="company_name"
                                           value="<?php echo isset($form_error) ? set_value("company_name") : "$item->company_name"; ?>">
                                    <?php if (isset($form_error)) { ?>
                                        <small class="pull-right input-form-error"> <?php echo form_error("company_name"); ?></small>
                                    <?php } ?>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label>Telefon 1</label>
                                    <input class="form-control" placeholder="Telefon numaranız" name="phone_1"
                                           value="<?php echo isset($form_error) ? set_value("phone_1") : "$item->phone_1"; ?>">
                                    <?php if (isset($form_error)) { ?>
                                        <small class="pull-right input-form-error"> <?php echo form_error("phone_1"); ?></small>
                                    <?php } ?>
                                </div>
                                
                                <div class="form-group col-md-4">
                                    <label>Telefon 2</label>
                                    <input class="form-control" placeholder="Diğer telefon numaranız (opsiyonel)" name="phone_2"
                                           value="<?php echo isset($form_error) ? set_value("phone_1") : "$item->phone_2"; ?>">
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label>Faks 1</label>
                                    <input class="form-control" placeholder="Faks numaranız" name="fax_1"
                                           value="<?php echo isset($form_error) ? set_value("phone_1") : "$item->fax_1"; ?>">
                                </div>
                                
                                <div class="form-group col-md-4">
                                    <label>Faks 2</label>
                                    <input class="form-control" placeholder="Diğer faks numaranız (opsiyonel)" name="fax_2"
                                           value="<?php echo isset($form_error) ? set_value("phone_1") : "$item->fax_2"; ?>">
                                </div>
                            </div>
                        </div><!-- .tab-pane  -->
                        
                        <div role="tabpanel" class="tab-pane fade" id="tab-2">
                            <div class="row">
                                <div class="form-group col-md-12" style="margin-bottom: 6px!important;">
                                    <label>Adres Bilgisi</label>
                                    <textarea name="address" class="m-0" data-plugin="summernote" data-options="{height: 250}">
                                        <?php echo isset($form_error) ? set_value("address") : "$item->address"; ?>
                                    </textarea>
                                </div>
                            </div>
                            <h3 style="margin-top: 0px;">Harita Koordinat Bilgileri</h3>
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <label>Enlem (Latitude)</label>
                                    <input class="form-control" placeholder="Enlem (Latitude)" name="latitude"
                                           value="<?php echo isset($form_error) ? set_value("latitude") : "$item->latitude"; ?>">
                                </div>

                                <div class="form-group col-md-3">
                                    <label>Boylam (Longtitude)</label>
                                    <input class="form-control" placeholder="Boylam (Longtitude)" name="longtitude"
                                           value="<?php echo isset($form_error) ? set_value("longtitude") : "$item->longtitude"; ?>">
                                </div>

                                <div class="form-group col-md-6">
                                    <label>Bbox</label>
                                    <input class="form-control" placeholder="Bbox" name="bbox"
                                           value="<?php echo isset($form_error) ? set_value("bbox") : "$item->bbox"; ?>">
                                </div>
                            </div>
                        </div><!-- .tab-pane  -->
                        
                        <div role="tabpanel" class="tab-pane fade" id="tab-3">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Hakkımızda</label>
                                    <textarea name="about_us" class="m-0" data-plugin="summernote" data-options="{height: 250}">
                                        <?php echo isset($form_error) ? set_value("about_us") : "$item->about_us"; ?>
                                    </textarea>
                                </div>
                            </div>
                        </div><!-- .tab-pane  -->
                        
                        <div role="tabpanel" class="tab-pane fade" id="tab-4">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Misyonumuz</label>
                                    <textarea name="mission" class="m-0" data-plugin="summernote" data-options="{height: 250}">
                                        <?php echo isset($form_error) ? set_value("mission") : "$item->mission"; ?>
                                    </textarea>
                                </div>
                            </div>
                        </div><!-- .tab-pane  -->
                        
                        <div role="tabpanel" class="tab-pane fade" id="tab-5">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Vizyonumuz</label>
                                    <textarea name="vision" class="m-0" data-plugin="summernote" data-options="{height: 250}">
                                        <?php echo isset($form_error) ? set_value("vision") : "$item->vision"; ?>
                                    </textarea>
                                </div>
                            </div>
                        </div><!-- .tab-pane  -->
                        
                        <div role="tabpanel" class="tab-pane fade" id="tab-6">
                            
                            <div class="row">
                                <div class="form-group col-md-8">
                                    <label>ePosta Adresiniz</label>
                                    <input class="form-control" placeholder="ePosta Adresiniz" name="email"
                                           value="<?php echo isset($form_error) ? set_value("email") : "$item->email"; ?>">
                                    <?php if (isset($form_error)) { ?>
                                        <small class="pull-right input-form-error"> <?php echo form_error("email"); ?></small>
                                    <?php } ?>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label>Facebook</label>
                                    <input class="form-control" placeholder="Facebook adresiniz" name="facebook"
                                           value="<?php echo isset($form_error) ? set_value("facebook") : "$item->facebook"; ?>">
                                </div>
                                
                                <div class="form-group col-md-4">
                                    <label>Twitter</label>
                                    <input class="form-control" placeholder="Twitter Adresiniz" name="twitter"
                                           value="<?php echo isset($form_error) ? set_value("twitter") : "$item->twitter"; ?>">
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label>Instagram</label>
                                    <input class="form-control" placeholder="Instagram adresiniz" name="instagram"
                                           value="<?php echo isset($form_error) ? set_value("instagram") : "$item->instagram"; ?>">
                                </div>

                                <div class="form-group col-md-4">
                                    <label>Linkedin</label>
                                    <input class="form-control" placeholder="Linkedin Adresiniz" name="linkedin"
                                           value="<?php echo isset($form_error) ? set_value("linkedin") : "$item->linkedin"; ?>">
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label>GitHub</label>
                                    <input class="form-control" placeholder="GitHub adresiniz" name="github"
                                           value="<?php echo isset($form_error) ? set_value("github") : "$item->github"; ?>">
                                </div>

                                <div class="form-group col-md-4">
                                    <label>GitLab</label>
                                    <input class="form-control" placeholder="GitLab Adresiniz" name="gitlab"
                                           value="<?php echo isset($form_error) ? set_value("gitlab") : "$item->gitlab"; ?>">
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label>Skype</label>
                                    <input class="form-control" placeholder="Skype adresiniz" name="skype"
                                           value="<?php echo isset($form_error) ? set_value("skype") : "$item->skype"; ?>">
                                </div>

                                <div class="form-group col-md-4">
                                    <label>Google Plus</label>
                                    <input class="form-control" placeholder="Google Plus Adresiniz" name="googleplus"
                                           value="<?php echo isset($form_error) ? set_value("googleplus") : "$item->googleplus"; ?>">
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label>WhatsApp</label>
                                    <input class="form-control" placeholder="WhatsApp adresiniz" name="whatsapp"
                                           value="<?php echo isset($form_error) ? set_value("whatsapp") : "$item->whatsapp"; ?>">
                                </div>
                            </div>
                        
                        </div><!-- .tab-pane  -->

                        <div role="tabpanel" class="tab-pane fade" id="tab-7">

                            <div class="row">
                                <div class="col-md-2">
                                    <img style="margin: 10% auto"
                                         src="<?php echo get_picture($viewFolder, $item->logo, "150x35"); ?>"
                                         alt="<?php echo $item->company_name; ?>"
                                         class="img-responsive">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Masaüstü Logo Seçimi</label>
                                    <input type="file" name="logo" class="form-control">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-2">
                                    <img style="margin: 10% auto"
                                         src="<?php echo get_picture($viewFolder, $item->logo_mobile, "140x33"); ?>"
                                         alt="<?php echo $item->company_name; ?>"
                                         class="img-responsive">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Mobil Logo Seçimi</label>
                                    <input type="file" name="logo_mobile" class="form-control">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-2">
                                    <img style="margin: 10% auto"
                                         width="40px"
                                         src="<?php echo get_picture($viewFolder, $item->favicon, "192x192"); ?>"
                                         alt="<?php echo $item->company_name; ?>"
                                         class="img-responsive">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Favicon Seçimi</label>
                                    <input type="file" name="favicon" class="form-control">
                                </div>
                            </div>
                        
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="tab-8">

                            <div class="row">
                                <div class="form-group col-md-8">
                                    <label>Ana Sayfa da gösterilmesini istediğiniz galeriyi seçiniz...</label>
                                    <select class="form-control" name="homepage_gallery">
                                        <?php foreach ($galleries as $gallery) { ?>
                                        <option value="<?php echo $gallery->id; ?>"
                                                <?php echo ($gallery->id == $item->homepage_gallery) ? "selected" : ""; ?>
                                        ><?php echo $gallery->gallery_name; ?></option>
                                        <?php } ?>
                                    </select>
                                    <?php if (isset($form_error)) { ?>
                                        <small class="pull-right input-form-error"> <?php echo form_error("homepage_gallery"); ?></small>
                                    <?php } ?>
                                </div>
                            </div>

                        </div><!-- .tab-pane  -->

                    </div><!-- .tab-content  -->
                </div><!-- .nav-tabs-horizontal -->
            </div><!-- .widget -->
            <div class="widget">
                <div class="m-b-lg col-md-12">
                    <button type="submit" class="btn btn-primary btn-md">
                        <i class="fa fa-refresh"></i>
                        Güncelle
                    </button>
                    
                    <a href="<?php echo base_url("settings"); ?>" class="btn btn-md btn-danger">
                        <i class="fa fa-times"></i>
                        İptal
                    </a>
                </div>
            </div>
        </form>
    </div><!-- END column -->
</div>