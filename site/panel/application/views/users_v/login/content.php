<div class="simple-page-wrap">
    <div class="simple-page-logo animated swing">
        <a href="<?php echo base_url("login"); ?>">
            <h1 style="font-family: Pacifico; color: #CCCCCC; text-shadow: 2px 2px 4px #444444">Beauty Center</h1>
            <p><i class="zmdi zmdi-check-all zmdi-hc-2x"></i></p>
            <p>İçerik Yönetim Sistemi</p>
        </a>
        </a>
    </div><!-- logo -->
    <div class="simple-page-form animated flipInY" id="login-form">
        <form action="<?php echo base_url("userop/do_login"); ?>" method="post">
            <div class="form-group">
                <input id="sign-in-email" type="email" class="form-control" placeholder="ePosta" name="user_email">
                <?php if (isset($form_error)) { ?>
                <small class="pull-right input-form-error"> <?php echo form_error("user_email"); ?></small>
                <?php } ?>
            </div>
            
            <div class="form-group">
                <input id="sign-in-password" type="password" class="form-control" placeholder="Şifre" name="user_password">
                <?php if (isset($form_error)) { ?>
                    <small class="pull-right input-form-error"> <?php echo form_error("user_password"); ?></small>
                <?php } ?>
            </div>
            
            <button type="submit" class="btn btn-purple">Giriş Yap</button>
        </form>
    </div><!-- #login-form -->
    
    <div class="simple-page-footer">
        <p><a href="<?php echo base_url("forgot-password"); ?>">Şifremi Unuttum</a></p>
    </div><!-- .simple-page-footer -->


</div><!-- .simple-page-wrap -->