<div class="simple-page-wrap">
    <div class="simple-page-logo animated swing">
        <a href="<?php echo base_url("login"); ?>">
            <h1 style="font-family: Pacifico; color: #CCCCCC ; text-shadow: 2px 2px 4px #444444">Beauty Center</h1>
            <p><i class="zmdi zmdi-check-all zmdi-hc-2x"></i></p>
            <p>İçerik Yönetim Sistemi</p>
        </a>
    </div><!-- logo -->
    <div class="simple-page-form animated flipInY" id="reset-password-form">
        <h4 class="form-title m-b-xl text-center">Şifrenizi mı unuttunuz? </h4>
        
        <form action="<?php echo base_url("reset-password") ?>" method="post">
            <div class="form-group">
                <input type="email" class="form-control" placeholder="ePosta Adresi" name="email"
                       value="<?php isset($form_error) ? set_value("email") : ""; ?>">
                <?php if (isset($form_error)) { ?>
                    <small class="pull-right input-form-error"> <?php echo form_error("email"); ?></small>
                <?php } ?>
            </div>
            <button type="submit" class="btn btn-purple">Yeni Şifre Gönder</button>
        </form>
</div><!-- .simple-page-wrap -->