<div class="row">
    <div class="col-md-12">
        <h4 class="m-b-lg">
            Hizmet Listesi
            <a href="<?php echo base_url("service/new_form"); ?>" class="btn btn-outline btn-primary btn-sm pull-right"> <i
                        class="fa fa-plus"></i> Yeni Ekle</a>
        </h4>
    </div><!-- END column -->
    <div class="col-md-12">
        <div class="widget p-lg">

            <?php if (empty($items)) { ?>

                <div class="alert alert-info text-center">
                    <p>Burada herhangi bir veri bulunmamaktadır. Eklemek için lütfen <a
                                href="<?php echo base_url("service/new_form"); ?>">tıklayınız</a></p>
                </div>

            <?php } else { ?>

                <table id="datatable-responsive" class="table table-hover table-striped table-bordered compact content-container" >
                    <thead>
                        <th class="order"><i class="fa fa-reorder"></i></th>
                        <th class="w50">#id</th>
                        <th>Başlık</th>
                        <th>url</th>
                        <th>Açıklama</th>
                        <th>Görsel</th>
                        <th class="w50">Durumu</th>
                        <th class="w175">İşlem</th>
                    </thead>

                    <tbody class="sortable" data-url="<?php echo base_url("service/rankSetter"); ?>">

                    <?php foreach ($items as $item) { ?>

                        <tr id="ord-<?php echo $item->id; ?>">
                            <td class="order"><i class="fa fa-reorder"></i></td>
                            <td class="w50 text-center">#<?php echo $item->id; ?></td>
                            <td><?php echo $item->title; ?></td>
                            <td><?php echo $item->url; ?></td>
                            <td><?php echo character_limiter(strip_tags($item->description), 100); ?></td>
                            <td class="text-center">
                                <img style="margin: 0 auto"
                                     width="75px"
                                     height="46.5px"
                                     src="<?php echo get_picture($viewFolder, $item->img_url, "350x217"); ?>"
                                     alt=""
                                     class="img-rounded"
                                >
                            </td>
                            <td class="text-center">
                                <input
                                        data-url="<?php echo base_url("service/isActiveSetter/$item->id"); ?>"
                                        class="isActive"
                                        type="checkbox"
                                        data-switchery
                                        data-color="#188ae2"
                                    <?php echo ($item->isActive) ? "checked" : ""; ?>
                                />
                            </td>
                            <td class="text-center">
                                <button
                                        data-url="<?php echo base_url("service/delete/$item->id"); ?>"
                                        class="btn btn-sm btn-danger btn-outline remove-btn">
                                    <i class="fa fa-trash"></i> Sil
                                </button>
                                <a href="<?php echo base_url("service/update_form/$item->id"); ?>">
                                    <button class="btn btn-sm btn-info btn-outline">
                                        <i class="fa fa-pencil-square-o"></i>
                                        Düzenle
                                    </button>
                                </a>
                            </td>
                        </tr>

                    <?php } ?>

                    </tbody>

                </table>

            <?php } ?>

        </div><!-- .widget -->
    </div><!-- END column -->
</div>