<div class="row">
    <div class="col-md-12">
        <h4 class="m-b-lg">
            Yeni Slide Ekle
        </h4>
    </div><!-- END column -->
    <div class="col-md-12">
        <div class="widget">
            <div class="widget-body">
                <form action="<?php echo base_url("slide/save"); ?>" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label>Başlık</label>
                        <input class="form-control" placeholder="Başlık" name="title"
                               value="<?php echo isset($form_error) ? set_value("title") : ""; ?>">
                        <?php if (isset($form_error)) { ?>
                            <small class="pull-right input-form-error"> <?php echo form_error("title"); ?></small>
                        <?php } ?>
                    </div>

                    <div class="form-group">
                        <label>Açıklama</label>
                        <textarea name="description" class="m-0" data-plugin="summernote"
                                  data-options="{height: 250}"></textarea>
                    </div>

                    <div class="form-group image_upload_container">
                        <label>Görsel Seçiniz</label>
                        <input type="file" name="img_url" class="form-control">
                    </div>

                    <div class="row">

                        <div class="form-group col-md-1 text-center">
                            <label>Buton Kullanımı</label><br>
                            <input
                                    class="form-control button_usage_btn"
                                    type="checkbox"
                                    data-switchery
                                    name="allowButton"
                                    data-color="#188ae2"
                            />
                        </div>

                        <div class="col-md-11">
                            <div class="button-information-container">
                                <div class="form-group">
                                    <label>Buton Başlığı</label>
                                    <input class="form-control" placeholder="Buton üzerinde yer alacak ibare"
                                           name="button_caption">
                                    <?php if (isset($form_error)) { ?>
                                        <small class="pull-right input-form-error"> <?php echo form_error("button_caption"); ?></small>
                                    <?php } ?>
                                </div>
                                <div class="form-group">
                                    <label>Buton URL Bilgisi</label>
                                    <input class="form-control"
                                           placeholder="Butona tıklandığında gidilmesi istenen link"
                                           name="button_url">
                                    <?php if (isset($form_error)) { ?>
                                        <small class="pull-right input-form-error"> <?php echo form_error("button_url"); ?></small>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary btn-md btn-outline">
                        <i class="fa fa-floppy-o"></i>
                        Kaydet
                    </button>
                    <a href="<?php echo base_url("slide"); ?>" class="btn btn-md btn-danger btn-outline">
                        <i class="fa fa-times"></i>
                        İptal
                    </a>
                </form>
            </div><!-- .widget-body -->
        </div><!-- .widget -->
    </div><!-- END column -->
</div>