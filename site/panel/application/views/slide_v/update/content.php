<div class="row">
    <div class="col-md-12">
        <h4 class="m-b-lg">
            <?php echo "<b>$item->title</b> kaydını düzenliyorsunuz"; ?>
        </h4>
    </div><!-- END column -->
    <div class="col-md-12">
        <div class="widget">
            <div class="widget-body">
                <form action="<?php echo base_url("slide/update/$item->id"); ?>" method="post"
                      enctype="multipart/form-data">
                    <div class="form-group">
                        <label>Başlık</label>
                        <input class="form-control" placeholder="Başlık" name="title"
                               value="<?php echo $item->title; ?>">
                        <?php if (isset($form_error)) { ?>
                            <small class="pull-right input-form-error"> <?php echo form_error("title"); ?></small>
                        <?php } ?>
                    </div>
                    <div class="form-group">
                        <label>Açıklama</label>
                        <textarea name="description" class="m-0" data-plugin="summernote"
                                  data-options="{height: 250}"><?php echo $item->description; ?></textarea>
                    </div>

                    <div class="row">

                        <div class="col-md-12 image_upload_container">
                            <img src="<?php echo get_picture($viewFolder, $item->img_url, "1920x650"); ?>"
                                 alt=""
                                 class="img-responsive img-rounded"
                                 style="margin: 0 auto; width: 30%; height: auto">
                        </div>

                        <div class="col-md-12 form-group image_upload_container">
                            <label>Görsel Seçiniz</label>
                            <input type="file" name="img_url" class="form-control">
                        </div>

                    </div>

                    <div class="row">

                        <div class="form-group col-md-1 text-center">
                            <label>Buton Kullanımı</label><br>
                            <input
                                    class="form-control button_usage_btn"
                                    type="checkbox"
                                    data-switchery
                                    name="allowButton"
                                    data-color="#188ae2"
                                    <?php echo ($item->allowButton) ? "checked" : ""; ?>
                            />
                        </div>

                        <div class="col-md-11">
                            <div class="button-information-container" style="display: <?php echo ($item->allowButton) ? "block" : "none"; ?>">
                                <div class="form-group">
                                    <label>Buton Başlığı</label>
                                    <input class="form-control" placeholder="Buton üzerinde yer alacak ibare"
                                           name="button_caption"
                                           value="<?php echo isset($form_error) ? set_value("button_caption") : $item->button_caption; ?>">
                                    <?php if (isset($form_error)) { ?>
                                        <small class="pull-right input-form-error"> <?php echo form_error("button_caption"); ?></small>
                                    <?php } ?>
                                </div>
                                <div class="form-group">
                                    <label>Buton URL Bilgisi</label>
                                    <input class="form-control"
                                           placeholder="Butona tıklandığında gidilmesi istenen link"
                                           name="button_url"
                                           value="<?php echo isset($form_error) ? set_value("button_url") : $item->button_url; ?>">
                                    <?php if (isset($form_error)) { ?>
                                        <small class="pull-right input-form-error"> <?php echo form_error("button_url"); ?></small>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row" style="padding: 12px;">
                        <button type="submit" class="btn btn-primary btn-md btn-outline">
                            <i class="fa fa-refresh"></i>
                            Güncelle
                        </button>
                        <a href="<?php echo base_url("slide"); ?>" class="btn btn-md btn-danger btn-outline">
                            <i class="fa fa-times"></i>
                            İptal</a>
                    </div>
                </form>
            </div><!-- .widget-body -->
        </div><!-- .widget -->
    </div><!-- END column -->
</div>