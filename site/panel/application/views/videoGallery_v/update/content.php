<div class="row">
    <div class="col-md-12">
        <h4 class="m-b-lg">
            <?php echo "<b>$item->gallery_name</b> kaydını düzenliyorsunuz"; ?>
        </h4>
    </div><!-- END column -->
    <div class="col-md-12">
        <div class="widget">
            <div class="widget-body">
                <form action="<?php echo base_url("videoGallery/update/$item->id"); ?>" method="post">
                    <div class="form-group">
                        <label>Galeri Adı</label>
                        <input class="form-control" placeholder="Galeri adını giriniz..." name="gallery_name" value="<?php echo $item->gallery_name; ?>">
                        <?php if(isset($form_error)){ ?>
                            <small class="pull-right input-form-error"> <?php echo form_error("gallery_name"); ?></small>
                        <?php } ?>
                    </div>
                    <button type="submit" class="btn btn-primary btn-md btn-outline">
                        <i class="fa fa-refresh" aria-hidden="true"></i>
                        Güncelle
                    </button>
                    <a href="<?php echo base_url("videoGallery"); ?>" class="btn btn-md btn-danger btn-outline">
                        <i class="fa fa-times" aria-hidden="true"></i>
                        İptal
                    </a>
                </form>
            </div><!-- .widget-body -->
        </div><!-- .widget -->
    </div><!-- END column -->
</div>