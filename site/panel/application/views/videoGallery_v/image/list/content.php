<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-10">
                <h4 class="m-b-lg">
                    <b><?php echo $item->gallery_name; ?></b> isimli galeriye ait videolar
                </h4>
            </div>
            <div class="col-md-2" style="text-align: right">
                <a href="<?php echo base_url("video/new_form/$item->id"); ?>">
                    <button class="btn btn-sm btn-outline btn-primary">
                        <i class="fa fa-plus"></i> Yeni Ekle
                    </button>
                </a>
                <a href="<?php echo base_url("videoGallery") ?>">
                    <button class="btn btn-sm btn-outline btn-inverse ml-3" >
                        <i class="fa fa-chevron-circle-left"></i> Geri Dön
                    </button>
                </a>
            </div>
        </div>

    </div><!-- END column -->
    <div class="col-md-12">
        <div class="widget p-lg">

            <?php if (empty($item_images)) { ?>

                <div class="alert alert-info text-center">
                    <p>Burada herhangi bir veri bulunmamaktadır. Eklemek için lütfen
                        <a href="<?php echo base_url("video/new_form"); ?>">tıklayınız</a>
                    </p>
                </div>

            <?php } else { ?>

                <table id="datatable-responsive"
                       class="table table-hover table-striped table-bordered content-container"
                >
                    <thead>
                    <th class="order"><i class="fa fa-reorder"></i></th>
                    <th class="w50">#id</th>
                    <th>url</th>
                    <th>Görsel</th>
                    <th class="w50">Durumu</th>
                    <th class="w175">İşlem</th>
                    </thead>
                    <tbody class="sortable" data-url="<?php echo base_url("video/rankSetter"); ?>">

                    <?php foreach ($item_images as $image) { ?>

                        <tr id="ord-<?php echo $image->id; ?>">
                            <td class="order"><i class="fa fa-reorder"></i></td>
                            <td class="w50 text-center">#<?php echo $image->id; ?></td>
                            <td><?php echo $image->video_url; ?></td>
                            <td class="w250">
                                <iframe
                                        width="250"
                                        height="auto"
                                        src="https://www.youtube.com/embed/<?php echo $image->video_url; ?>"
                                        frameborder="0"
                                        allow="autoplay; encrypted-media"
                                        allowfullscreen
                                >
                                </iframe>
                            </td>
                            <td class="text-center">
                                <input
                                        data-url="<?php echo base_url("video/isActiveSetter/$image->id"); ?>"
                                        class="isActive"
                                        type="checkbox"
                                        data-switchery
                                        data-color="#188ae2"
                                    <?php echo ($image->isActive) ? "checked" : ""; ?>
                                />
                            </td>
                            <td class="text-center">
                                <button
                                        data-url="<?php echo base_url("video/delete/$image->id/$image->gallery_id"); ?>"
                                        class="btn btn-sm btn-danger btn-outline remove-btn">
                                    <i class="fa fa-trash"></i> Sil
                                </button>
                                <a href="<?php echo base_url("video/update_form/$image->id"); ?>">
                                    <button class="btn btn-sm btn-info btn-outline">
                                        <i class="fa fa-pencil-square-o"></i>
                                        Düzenle
                                    </button>
                                </a>
                            </td>
                        </tr>

                    <?php } ?>

                    </tbody>

                </table>

            <?php } ?>

        </div><!-- .widget -->
    </div><!-- END column -->
</div>