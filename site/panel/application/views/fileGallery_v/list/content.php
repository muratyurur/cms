<div class="row">
    <div class="col-md-12">
        <h4 class="m-b-lg">
            Dosya Galerisi Listesi
            <a href="<?php echo base_url("fileGallery/new_form"); ?>"
               class="btn btn-outline btn-primary btn-sm pull-right"> <i class="fa fa-plus"></i> Yeni Ekle</a>
        </h4>
    </div><!-- END column -->
    <div class="col-md-12">
        <div class="widget p-lg">

            <?php if (empty($items)) { ?>

                <div class="alert alert-info text-center">
                    <p>Burada herhangi bir veri bulunmamaktadır. Eklemek için lütfen <a
                                href="<?php echo base_url("fileGallery/new_form"); ?>">tıklayınız</a></p>
                </div>

            <?php } else { ?>

                <table id="datatable-responsive"
                       class="table table-hover table-striped table-bordered content-container"
                >
                    <thead>
                    <th class="order"><i class="fa fa-reorder"></i></th>
                    <th class="w50">#id</th>
                    <th>Galeri Adı</th>
                    <th>Klasör Adı</th>
                    <th>URL</th>
                    <th class="w50">Durumu</th>
                    <th class="w275">İşlem</th>
                    </thead>
                    <tbody>

                    <?php foreach ($items as $item) { ?>

                        <tr id="ord-<?php echo $item->id; ?>">
                            <td class="order"><i class="fa fa-reorder"></i></td>
                            <td class="w50 text-center">#<?php echo $item->id; ?></td>
                            <td><?php echo $item->gallery_name; ?></td>
                            <td><?php echo $item->folder_name; ?></td>
                            <td><?php echo $item->url; ?></td>
                            <td class="text-center">
                                <input
                                        data-url="<?php echo base_url("fileGallery/isActiveSetter/$item->id"); ?>"
                                        class="isActive"
                                        type="checkbox"
                                        data-switchery
                                        data-color="#188ae2"
                                    <?php echo ($item->isActive) ? "checked" : ""; ?>
                                />
                            </td>
                            <td class="text-center">
                                <button data-url="<?php echo base_url("fileGallery/delete/$item->id"); ?>"
                                        class="btn btn-sm btn-danger btn-outline remove-btn">
                                    <i class="fa fa-trash"></i>
                                    Sil
                                </button>
                                <a href="<?php echo base_url("fileGallery/update_form/$item->id"); ?>">
                                    <button class="btn btn-sm btn-info btn-outline">
                                        <i class="fa fa-pencil-square-o"></i>
                                        Düzenle
                                    </button>
                                </a>
                                <a href="<?php echo base_url("fileGallery/file_form/$item->id"); ?>">
                                    <button class="btn btn-sm btn-dark btn-outline">
                                        <i class="fa fa-folder-open"></i>
                                        Galeri İçeriği
                                    </button>
                                </a>
                            </td>
                        </tr>

                    <?php } ?>

                    </tbody>

                </table>

            <?php } ?>

        </div><!-- .widget -->
    </div><!-- END column -->
</div>