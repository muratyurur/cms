<?php if (empty($item_files)) { ?>

    <div class="alert alert-info text-center">
        <p>Bu galeride herhangi bir dosya bulunmamaktadır.</a></p>
    </div>

<?php } else { ?>

    <table id="datatable-responsive"
           class="table table-hover table-striped table-bordered content-container"
    >
        <thead>
            <th class="order"><i class="fa fa-reorder"></i></th>
            <th>#id</th>
            <th>Görsel</th>
            <th>Dosya Adı</th>
            <th>URL</th>
            <th>Durumu</th>
            <th>İşlem</th>
        </thead>
        <tbody class="sortable" data-url="<?php echo base_url("fileGallery/fileRankSetter"); ?>">

        <?php foreach ($item_files as $file) { ?>

            <tr id="ord-<?php echo $file->id; ?>">
                <td class="order"><i class="fa fa-reorder"></i></td>
                <td class="w50 text-center">#<?php echo $file->id; ?></td>
                <td class="w100 text-center">
                    <img src="<?php echo base_url("assets/assets/images/32x32.png"); ?>" alt="file-gallery">
<!--                    <i class="fa fa-folder-open fa-2x"></i>-->
                </td>
                <td><?php echo $file->file_name; ?></td>
                <td><?php echo $file->file_url; ?></td>
                <td class="w100 text-center">
                    <input
                            data-url="<?php echo base_url("fileGallery/fileIsActiveSetter/$file->id"); ?>"
                            class="isActive"
                            type="checkbox"
                            data-switchery
                            data-color="#188ae2"
                        <?php echo ($file->isActive) ? "checked" : ""; ?>
                    />
                </td>
                <td class="w100 text-center">
                    <button
                            data-url="<?php echo base_url("fileGallery/fileDelete/$file->id/$file->gallery_id"); ?>"
                            class="btn btn-sm btn-danger btn-outline remove-btn btn-block">
                        <i class="fa fa-trash"></i> Sil
                    </button>
                </td>
            </tr>

        <?php } ?>

        </tbody>

    </table>
<?php } ?>