<?php $permissions = json_decode($item->permissions); ?>

<div class="row">
    <div class="col-md-12">
        <h4 class="m-b-lg">
            <?php echo "<b>$item->title</b> rolüne ait yetkileri düzenliyorsunuz"; ?>
            <a href="<?php echo base_url("user_roles") ?>">
                <button class="btn btn-sm btn-outline btn-inverse ml-3 pull-right">
                    <i class="fa fa-chevron-circle-left"></i> Geri Dön
                </button>
            </a>
        </h4>
    </div><!-- END column -->
    <div class="col-md-12">
        <div class="widget">
            <div class="widget-body">
                <form action="<?php echo base_url("user_roles/update_permissions/$item->id"); ?>" method="post">
                    <table id="datatable-responsive" class="table table-hover table-striped table-bordered content-container">
                        <thead>
                            <th>Modül Adı</th>
                            <th class="w75">Görüntüleme</th>
                            <th class="w75">Ekleme</th>
                            <th class="w75">Düzenleme</th>
                            <th class="w75">Silme</th>
                        </thead>
                        <tbody>
                        <?php foreach (get_controller_list() as $controllerName) { ?>
                            <tr>
                                <td><?php echo $controllerName; ?></td>
                                <td class="text-center">
                                    <input
                                            <?php echo (isset($permissions->$controllerName) && isset($permissions->$controllerName->read)) ? "checked" : ""; ?>
                                            name="permissions[<?php echo $controllerName; ?>][read]" type="checkbox" data-switchery data-color="#188ae2"/>
                                </td>
                                <td class="text-center">
                                    <input
                                            <?php echo (isset($permissions->$controllerName) && isset($permissions->$controllerName->write)) ? "checked" : ""; ?>
                                            name="permissions[<?php echo $controllerName; ?>][write]" type="checkbox" data-switchery data-color="#188ae2"/>
                                </td>
                                <td class="text-center">
                                    <input
                                            <?php echo (isset($permissions->$controllerName) && isset($permissions->$controllerName->update)) ? "checked" : ""; ?>
                                            name="permissions[<?php echo $controllerName; ?>][update]" type="checkbox" data-switchery data-color="#188ae2"/>
                                </td>
                                <td class="text-center">
                                    <input
                                            <?php echo (isset($permissions->$controllerName) && isset($permissions->$controllerName->delete)) ? "checked" : ""; ?>
                                            name="permissions[<?php echo $controllerName; ?>][delete]" type="checkbox" data-switchery data-color="#188ae2"/>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                    <hr>
                    <div class="row" style="padding: 0px 12px;">
                        <button type="submit" class="btn btn-primary btn-md btn-outline">
                            <i class="fa fa-refresh"></i>
                            Güncelle
                        </button>
                        <a href="<?php echo base_url("user_roles"); ?>" class="btn btn-md btn-danger btn-outline">
                            <i class="fa fa-times"></i>
                            İptal</a>
                    </div>
                </form>
            </div><!-- .widget-body -->
        </div><!-- .widget -->
    </div><!-- END column -->
</div>