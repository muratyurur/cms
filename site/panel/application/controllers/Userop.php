<?php
    
    class Userop extends CI_Controller
    {
        public $viewFolder = "";
        
        public function __construct()
        {
            parent::__construct();
            
            $this->viewFolder = "users_v";
            
            $this->load->model("user_model");
        }
        
        public function login()
        {
            if (get_active_user()) {
                redirect(base_url());
            }
            
            $viewData = new stdClass();
            
            $this->load->library("form_validation");
            
            /** View'e gönderilecek Değişkenlerin Set Edilmesi.. */
            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "login";
            
            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
        }
        
        public function do_login()
        {
            if (get_active_user()) {
                redirect(base_url());
            }
            
            $this->load->library("form_validation");
            
            // Kurallar yazilir..
            
            $this->form_validation->set_rules("user_email", "ePosta", "required|trim|valid_email");
            
            $this->form_validation->set_rules("user_password", "Şifre", "required|trim");
            
            $this->form_validation->set_message(
                array(
                    "required" => "<b>{field}</b> alanı boş bırakılamaz.",
                    "valid_email" => "Lütfen geçerli bir eposta adresi giriniz.",
                )
            );
            
            // Form Validation Calistirilir..
            if ($validate = $this->form_validation->run() == false) {
                
                $viewData = new stdClass();
                
                /** View'e gönderilecek Değişkenlerin Set Edilmesi.. */
                $viewData->viewFolder = $this->viewFolder;
                $viewData->subViewFolder = "login";
                $viewData->form_error = true;
                
                $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
                
            }
            else {
                
                $user = $this->user_model->get(
                    array(
                        "email" => $this->input->post("user_email"),
                        "password" => md5($this->input->post("user_password")),
                        "isActive" => 1
                    )
                );
                
                if ($user) {
                    
                    $alert = array(
                        "title" => "İşlem başarılı",
                        "text" => "$user->full_name hoşgeldiniz...",
                        "type" => "success"
                    );

                    setUserRoles();
                    
                    $this->session->set_userdata("user", $user);
                    $this->session->set_flashdata("alert", $alert);
                    
                    redirect(base_url());
                    
                }
                else {
                    
                    $alert = array(
                        "title" => "İşlem başarısız",
                        "text" => "Lütfen giriş bilgilerinizi kontrol ediniz...",
                        "type" => "error"
                    );
                    
                    $this->session->set_flashdata("alert", $alert);
                    
                    redirect(base_url("login"));
                }
                
            }
            
        }
        
        public function logout()
        {
            $this->session->unset_userdata("user");
            
            redirect(base_url());
        }
        
        public function forgot_password()
        {
            if (get_active_user()) {
                redirect(base_url());
            }
            
            $viewData = new stdClass();
            
            $this->load->library("form_validation");
            
            /** View'e gönderilecek Değişkenlerin Set Edilmesi.. */
            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "forgot_password";
            
            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
        }
        
        public function reset_password()
        {
            $this->load->library("form_validation");
            
            // Kurallar yazilir..
            
            $this->form_validation->set_rules("email", "ePosta", "required|trim|valid_email");
            
            $this->form_validation->set_message(
                array(
                    "required" => "<b>{field}</b> alanı boş bırakılamaz.",
                    "valid_email" => "Lütfen geçerli bir eposta adresi giriniz.",
                )
            );
            
            if ($this->form_validation->run() === false) {
                
                $viewData = new stdClass();
                
                /** View'e gönderilecek Değişkenlerin Set Edilmesi.. */
                $viewData->viewFolder = $this->viewFolder;
                $viewData->subViewFolder = "forgot_password";
                $viewData->form_error = true;
                
                $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
                
            }
            else {
                
                $user = $this->user_model->get(
                    array(
                        "isActive" => 1,
                        "email" => $this->input->post("email")
                    )
                );
                
                if ($user) {
                    
                    $this->load->helper("string");
                    
                    $temp_password = random_string();
                    
                    $message_body = "
<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\">
<head>
	<!--[if gte mso 9]>
	<xml>
		<o:OfficeDocumentSettings>
		<o:AllowPNG/>
		<o:PixelsPerInch>96</o:PixelsPerInch>
		</o:OfficeDocumentSettings>
	</xml>
	<![endif]-->
	<meta http-equiv=\"Content-type\" content=\"text/html; charset=utf-8\" />
	<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\" />
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />
	<meta name=\"format-detection\" content=\"date=no\" />
	<meta name=\"format-detection\" content=\"address=no\" />
	<meta name=\"format-detection\" content=\"telephone=no\" />
	<meta name=\"x-apple-disable-message-reformatting\" />
    <!--[if !mso]><!-->
	<link href=\"https://fonts.googleapis.com/css?family=Noto+Sans:400,400i,700,700i\" rel=\"stylesheet\" />
	<link href=\"https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i\" rel=\"stylesheet\">
    <!--<![endif]-->
	<title>Şifre Sıfırlama</title>
	<!--[if gte mso 9]>
	<style type=\"text/css\" media=\"all\">
		sup { font-size: 100% !important; }
	</style>
	<![endif]-->
	

	<style type=\"text/css\" media=\"screen\">
		/* Linked Styles */
		body { padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#f4f4f4; -webkit-text-size-adjust:none }
		a { color:#66c7ff; text-decoration:none }
		p { padding:0 !important; margin:0 !important }
		img { -ms-interpolation-mode: bicubic; /* Allow smoother rendering of resized image in Internet Explorer */ }
		.mcnPreviewText { display: none !important; }

		
		/* Mobile styles */
		@media only screen and (max-device-width: 480px), only screen and (max-width: 480px) {
			.mobile-shell { width: 100% !important; min-width: 100% !important; }
			.bg { background-size: 100% auto !important; -webkit-background-size: 100% auto !important; }
			
			.text-header,
			.m-center { text-align: center !important; }
			
			.center { margin: 0 auto !important; }
			.container { padding: 20px 10px !important }
			
			.td { width: 100% !important; min-width: 100% !important; }

			.m-br-15 { height: 15px !important; }
			.p30-15 { padding: 30px 15px !important; }
			.p40 { padding: 20px !important; }

			.m-td,
			.m-hide { display: none !important; width: 0 !important; height: 0 !important; font-size: 0 !important; line-height: 0 !important; min-height: 0 !important; }

			.m-block { display: block !important; }

			.fluid-img img { width: 100% !important; max-width: 100% !important; height: auto !important; }

			.column,
			.column-top,
			.column-empty,
			.column-empty2,
			.column-dir-top { float: left !important; width: 100% !important; display: block !important; }
			.column-empty { padding-bottom: 10px !important; }
			.column-empty2 { padding-bottom: 20px !important; }
			.content-spacing { width: 15px !important; }
		}
	</style>
</head>
<body class=\"body\" style=\"padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#f4f4f4; -webkit-text-size-adjust:none;\">
	<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"#f4f4f4\">
		<tr>
			<td align=\"center\" valign=\"top\">
				<table width=\"650\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"mobile-shell\">
					<tr>
						<td class=\"td container\" style=\"width:650px; min-width:650px; font-size:0pt; line-height:0pt; margin:0; font-weight:normal; padding:55px 0px;\">
							<!-- Header -->
							<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
								<tr>
									<td style=\"padding-bottom: 20px;\">
										<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
											<tr>
												<td class=\"p30-15\" style=\"padding: 25px 30px 25px 30px;\" bgcolor=\"#ffffff\">
													<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
														<tr>
															<th class=\"column-top\" width=\"145\" style=\"font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;\">
																<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
																	<tr>
																		<td class=\"img m-center\" style=\"font-size:0pt; line-height:0pt; text-align:center;\"><img src=\"https://cms.muratyurur.com/panel/assets/assets/images/banner.png\" border=\"0\" alt=\"\" style=\"padding-bottom: 10px\" /></td>
																	</tr>
																	<tr>
																		<td class=\"text pb20\" style=\"color:#777777; font-family:'Lato', Arial,sans-serif; font-size:25px; line-height:26px; text-align:center;\">
																			<p>İçerik Yönetim Sistemi<p>
																			<p style=\"color:#777777; font-family:'Montserrat', Arial,sans-serif; font-size:12px; line-height:26px; text-align:center;\">Şifre Sıfırlama Talebi</p>
																		</td>
																	</tr>
																</table>
															</th>
															<th class=\"column-empty\" width=\"1\" style=\"font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;\"></th>
															
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
							<!-- END Header -->

							<!-- Intro -->
							<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
								<tr>
									<td style=\"padding-bottom: 20px;\">
										<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
											<tr>
												<td background=\"https://cms.muratyurur.com/panel/assets/assets/images/forgot_password.jpg\"
												bgcolor=\"#114490\" valign=\"top\" height=\"366\" class=\"bg\"
												style=\"background-size:cover !important; -webkit-background-size:cover!important;
												background-repeat:none;\">
													<!--[if gte mso 9]>
													<v:rect xmlns:v=\"urn:schemas-microsoft-com:vml\" fill=\"true\" stroke=\"false\" style=\"width:650px; height: 366px\">
														<v:fill type=\"frame\" src=\"images/t8_bg.jpg\" color=\"#114490\" />
														<v:textbox inset=\"0,0,0,0\">
													<![endif]-->
													<div>
														<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
															<tr>
																<td class=\"content-spacing\" width=\"30\" height=\"366\" style=\"font-size:0pt; line-height:0pt; text-align:left;\"></td>
																<td class=\"content-spacing\" width=\"30\" style=\"font-size:0pt; line-height:0pt; text-align:left;\"></td>
															</tr>
														</table>
													</div>
													<!--[if gte mso 9]>
														</v:textbox>
														</v:rect>
													<![endif]-->
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
							<!-- END Intro -->

							<!-- Article / Title + Copy + Button -->
							<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
								<tr>
									<td style=\"padding-bottom: 20px;\">
										<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"#ffffff\">
											<tr>
												<td class=\"p30-15\" style=\"padding: 50px 30px;\">
													<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
														<tr>
															<td class=\"h3 pb20\" style=\"color:#5b69bc; font-family:'Montserrat', Arial,sans-serif; font-size:24px; line-height:32px; text-align:center; padding-bottom:20px;\">Merhaba <b>$user->full_name</b>;</td>
														</tr>
														<tr>
															<td class=\"text pb20\" style=\"color:#777777; font-family:'Montserrat', Arial,sans-serif; font-size:14px; line-height:26px; text-align:justify; padding-bottom:20px;\"><b>$user->user_name</b> kullanıcı adınıza ait <a href=\"http://muratyurur.com\" style=\"color: #5b69bc; font-family:'Montserrat', Arial,sans-serif; text-align: center; text-decoration: none\">muratyurur.com</a> içerik yönetim sistemi
																şifrenizi değiştirmek için bir talepte bulundunuz. Geçici olarak giriş yapmanıza olanak sağlayacak olan şifreniz:</td>
														</tr>
														<tr>
															<td class=\"text pb20\" style=\"color:tomato; font-family:'Montserrat', Arial,sans-serif; font-size:14px; line-height:26px; text-align:center; padding-bottom:20px;\">
																<h1>$temp_password</h1>
															</td>
														</tr>
														<tr>
															<td class=\"text pb20\" style=\"color:#777777; font-family:'Montserrat', Arial,sans-serif; font-size:14px; line-height:26px; text-align:justify; padding-bottom:20px;\">
																Şifre değişikliği talebi size ait değilse ya da şüpheli işlem olduğunu düşünüyorsanız
																lütfen sistem yöneticisi ile irtibata geçiniz.
																<br>
																<br>
																Sisteme giriş yapmak için lütfen <a href=\"https://cms.muratyurur.com/panel\" style=\"color: #5b69bc; font-family:'Montserrat', Arial,sans-serif; text-align: center; text-decoration: none\">tıklayın</a>.
															</td>
														</tr>
														<tr>
															<td class=\"text pb20\" style=\"color:#777777; font-family:'Montserrat', Arial,sans-serif; font-size:14px; line-height:26px; text-align:center; padding-bottom:20px;\">
																<h4 style=\"font-family:'Montserrat', Arial,sans-serif;\">Bir sorunuz mu var? Lütfen sormaktan çekinmeyin!</h4>
																<a href=\"mailto:admin@muratyurur.com\" style=\"color: #5b69bc; font-family:'Montserrat', Arial,sans-serif; text-align: center; text-decoration: none\">admin@muratyurur.com</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
							<!-- END Article / Title + Copy + Button -->
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>
";
                    
                    $send = send_mail($user->email, "Şifre sıfırlama talebi", $message_body);
                    
                    if ($send) {
                        
                        $this->user_model->update(
                            array(
                                "id" => $user->id
                            ),
                            array(
                                "password" => md5($temp_password)
                            )
                        );
                        
                        $alert = array(
                            "title" => "İşlem başarılı",
                            "text" => "Geçici şifreniz <b>{$user->email}</b> adresine başarıyla gönderildi.",
                            "type" => "success"
                        );
                        
                        $this->session->set_flashdata("alert", $alert);
                        
                        redirect(base_url("login"));
                    }
                    else {
                        echo $this->email->print_debugger();
                    }
                    
                }
                else {
                    
                    $alert = array(
                        "title" => "İşlem başarısız",
                        "text" => "Sistemde kayıtlı böyle bir kullanıcı hesabı bulunmamaktadır.",
                        "type" => "error"
                    );
                    
                    $this->session->set_flashdata("alert", $alert);
                    
                    redirect(base_url("userop/forgot_password"));
                    
                }
                
            }
            
            $this->input->post("email");
            
        }
    }