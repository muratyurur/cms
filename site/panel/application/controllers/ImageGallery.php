<?php

class ImageGallery extends MY_Controller
{
    public $viewFolder = "";

    public function __construct()
    {

        parent::__construct();

        $this->viewFolder = "imageGallery_v";

        $this->load->model("imageGallery_model");
        $this->load->model("image_model");
    
        if (!get_active_user()){
            redirect(base_url("login"));
        }

    }

    public function index(){

        $viewData = new stdClass();

        /** Tablodan Verilerin Getirilmesi.. */
        $items = $this->imageGallery_model->get_all(
            array()
        );

        /** View'e gönderilecek Değişkenlerin Set Edilmesi.. */
        $viewData->viewFolder = $this->viewFolder;
        $viewData->subViewFolder = "list";
        $viewData->items = $items;

        $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
    }

    public function new_form(){

        $viewData = new stdClass();

        /** View'e gönderilecek Değişkenlerin Set Edilmesi.. */
        $viewData->viewFolder = $this->viewFolder;
        $viewData->subViewFolder = "add";

        $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);

    }

    public function save(){

        $this->load->library("form_validation");

        // Kurallar yazilir..
        $this->form_validation->set_rules("gallery_name", "Galeri Adı", "required|trim");

        $this->form_validation->set_message(
            array(
                "required"  => "<b>{field}</b> alanı boş bırakılamaz."
            )
        );

        // Form Validation Calistirilir..
        // TRUE - FALSE
        $validate = $this->form_validation->run();

        // Monitör Askısı
        // monitor-askisi

        if($validate){

            $folder_name    = convertToSEO($this->input->post("gallery_name"));
            $path           = "uploads/$this->viewFolder/$folder_name";

            $create_folder = mkdir($path, 0755);

            $insert = $this->imageGallery_model->add(
                array(
                    "gallery_name"  => $this->input->post("gallery_name"),
                    "url"           => $folder_name,
                    "folder_name"   => convertToSEO($this->input->post("gallery_name")),
                    "isActive"      => 1,
                    "createdAt"     => date("Y-m-d H:i:s")
                )
            );

            if($insert){

                $alert = array(
                    "title" => "İşlem Başarılı",
                    "text"  => "Kayıt başarılı bir şekilde eklendi",
                    "type"  => "success"
                );

            } else {

                $alert = array(
                    "title" => "İşlem Başarısız",
                    "text"  => "Kayıt Ekleme sırasında bir problem oluştu",
                    "type"  => "error"
                );
            }

            // İşlemin Sonucunu Session'a yazma işlemi...
            $this->session->set_flashdata("alert", $alert);

            redirect(base_url("imageGallery"));

        } else {

            $viewData = new stdClass();

            /** View'e gönderilecek Değişkenlerin Set Edilmesi.. */
            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "add";
            $viewData->form_error = true;

            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
        }

        // Başarılı ise
            // Kayit işlemi baslar
        // Başarısız ise
            // Hata ekranda gösterilir...

    }

    public function update_form($id){

        $viewData = new stdClass();

        /** Tablodan Verilerin Getirilmesi.. */
        $item = $this->imageGallery_model->get(
            array(
                "id"    => $id,
            )
        );
        
        /** View'e gönderilecek Değişkenlerin Set Edilmesi.. */
        $viewData->viewFolder = $this->viewFolder;
        $viewData->subViewFolder = "update";
        $viewData->item = $item;

        $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);


    }

    public function update($id, $oldFolderName){

        $this->load->library("form_validation");

        // Kurallar yazilir..
        $this->form_validation->set_rules("gallery_name", "Galeri Adı", "required|trim");

        $this->form_validation->set_message(
            array(
                "required"  => "<b>{field}</b> alanı boş bırakılamaz."
            )
        );

        // Form Validation Calistirilir..
        // TRUE - FALSE
        $validate = $this->form_validation->run();

        // Monitör Askısı
        // monitor-askisi

        if($validate){

            $folder_name    = convertToSEO($this->input->post("gallery_name"));
            $path           = "uploads/$this->viewFolder/";

            rename("$path/$oldFolderName", "$path/$folder_name");

            $update = $this->imageGallery_model->update(
                array(
                    "id"    => $id
                ),
                array(
                    "gallery_name"  => $this->input->post("gallery_name"),
                    "folder_name"   => $folder_name,
                    "url"           => convertToSEO($this->input->post("gallery_name")),
                )
            );

            // TODO Alert sistemi eklenecek...
            if($update){

                $alert = array(
                    "title" => "İşlem Başarılı",
                    "text"  => "Kayıt başarılı bir şekilde güncellendi",
                    "type"  => "success"
                );

            } else {

                $alert = array(
                    "title" => "İşlem Başarısız",
                    "text"  => "Güncelleme sırasında bir problem oluştu",
                    "type"  => "error"
                );


            }

            $this->session->set_flashdata("alert", $alert);
            redirect(base_url("imageGallery"));

        } else {

            $viewData = new stdClass();

            /** Tablodan Verilerin Getirilmesi.. */
            $item = $this->imageGallery_model->get(
                array(
                    "id"    => $id,
                )
            );

            /** View'e gönderilecek Değişkenlerin Set Edilmesi.. */
            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "update";
            $viewData->form_error = true;
            $viewData->item = $item;

            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
        }

        // Başarılı ise
        // Kayit işlemi baslar
        // Başarısız ise
        // Hata ekranda gösterilir...

    }

    public function delete($id){

        $folderName = $this->imageGallery_model->get(
            array(
                "id"    => $id
            )
        );

        $delete = $this->imageGallery_model->delete(
            array(
                "id"    => $id
            )
        );

        // TODO Alert Sistemi Eklenecek...
        if($delete){

            rrmdir("uploads/$this->viewFolder/$folderName->folder_name");

            $alert = array(
                "title" => "İşlem Başarılı",
                "text"  => "Kayıt başarılı bir şekilde silindi",
                "type"  => "success"
            );

        } else {

            $alert = array(
                "title" => "İşlem Başarısız",
                "text"  => "Kayıt silme sırasında bir problem oluştu",
                "type"  => "error"
            );


        }

        $this->session->set_flashdata("alert", $alert);
        redirect(base_url("imageGallery"));


    }

    public function imageDelete($id, $parent_id){

        $fileName = $this->image_model->get(
            array(
                "id"    => $id
            )
        );

        $folderName = $this->imageGallery_model->get(
            array(
                "id"    => $parent_id
            )
        );

        $delete = $this->image_model->delete(
            array(
                "id"    => $id
            )
        );


        // TODO Alert Sistemi Eklenecek...
        if($delete){

            unlink("uploads/imageGallery_v/$folderName->folder_name/1140x705/$fileName->image_url");
            unlink("uploads/imageGallery_v/$folderName->folder_name/253x156/$fileName->image_url");
            unlink("uploads/imageGallery_v/$folderName->folder_name/350x216/$fileName->image_url");
            unlink("uploads/imageGallery_v/$folderName->folder_name/540x334/$fileName->image_url");
            unlink("uploads/imageGallery_v/$folderName->folder_name/750x464/$fileName->image_url");

            redirect(base_url("imageGallery/image_form/$parent_id"));
        } else {
            redirect(base_url("imageGallery/image_form/$parent_id"));
        }

    }

    public function imageDeleteAll($parent_id){

        $folderName = $this->imageGallery_model->get(
            array(
                "id"    => $parent_id
            )
        );

        $delete = $this->image_model->delete(
            array(
                "gallery_id"    => $parent_id
            )
        );


        // TODO Alert Sistemi Eklenecek...
        if($delete){

            rrmdir("uploads/{$this->viewFolder}/$folderName->folder_name/1140x705");
            rrmdir("uploads/{$this->viewFolder}/$folderName->folder_name/253x156");
            rrmdir("uploads/{$this->viewFolder}/$folderName->folder_name/350x216");
            rrmdir("uploads/{$this->viewFolder}/$folderName->folder_name/540x334");
            rrmdir("uploads/{$this->viewFolder}/$folderName->folder_name/750x464");

            redirect(base_url("imageGallery/image_form/$parent_id"));
        } else {
            redirect(base_url("imageGallery/image_form/$parent_id"));
        }

    }

    public function isActiveSetter($id){

        if($id){

            $isActive = ($this->input->post("data") === "true") ? 1 : 0;

            $this->imageGallery_model->update(
                array(
                    "id"    => $id
                ),
                array(
                    "isActive"  => $isActive
                )
            );
        }
    }

    public function imageIsActiveSetter($id){

        if($id){

            $isActive = ($this->input->post("data") === "true") ? 1 : 0;

            $this->image_model->update(
                array(
                    "id"    => $id
                ),
                array(
                    "isActive"  => $isActive
                )
            );
        }
    }

    public function imageRankSetter(){


        $data = $this->input->post("data");

        parse_str($data, $order);

        $items = $order["ord"];

        foreach ($items as $rank => $id){

            $this->image_model->update(
                array(
                    "id"        => $id,
                    "rank !="   => $rank
                ),
                array(
                    "rank"      => $rank
                )
            );

        }

    }

    public function image_form($id){

        $viewData = new stdClass();

        /** View'e gönderilecek Değişkenlerin Set Edilmesi.. */
        $viewData->viewFolder = $this->viewFolder;
        $viewData->subViewFolder = "image";

        $viewData->item = $this->imageGallery_model->get(
            array(
                "id"    => $id
            )
        );

        $viewData->item_images = $this->image_model->get_all(
            array(
                "gallery_id"    => $id
            ), "rank ASC"
        );

        $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
    }

    public function image_upload($gallery_id){

        $this->load->model("imageGallery_model");

        $image = $this->imageGallery_model->get(
            array(
                "id"    => $gallery_id
            )
        );

        $file_name = $image->url . "-" . convertToSEO(pathinfo($_FILES["file"]["name"], PATHINFO_FILENAME)) . "." . pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION);

        $image_350x216 = upload_picture($_FILES["file"]["tmp_name"], "uploads/$this->viewFolder/$image->folder_name/", 350, 216, $file_name);
        $image_253x156 = upload_picture($_FILES["file"]["tmp_name"], "uploads/$this->viewFolder/$image->folder_name/", 253, 156, $file_name);
        $image_1140x705 = upload_picture($_FILES["file"]["tmp_name"], "uploads/$this->viewFolder/$image->folder_name/", 1140, 705, $file_name);
        $image_540x334 = upload_picture($_FILES["file"]["tmp_name"], "uploads/$this->viewFolder/$image->folder_name/", 540, 334, $file_name);
        $image_750x464 = upload_picture($_FILES["file"]["tmp_name"], "uploads/$this->viewFolder/$image->folder_name/", 750, 464, $file_name);

        if ($image_350x216 && $image_253x156 && $image_1140x705 && $image_540x334 && $image_750x464) {

            $this->image_model->add(
                array(
                    "image_url"     => $file_name,
                    "rank"          => 0,
                    "isActive"      => 1,
                    "createdAt"     => date("Y-m-d H:i:s"),
                    "gallery_id"    => $gallery_id
                )
            );

        } else {
            echo "islem basarisiz";
        }

    }

    public function refresh_image_list($id){

        $viewData = new stdClass();

        /** View'e gönderilecek Değişkenlerin Set Edilmesi.. */
        $viewData->viewFolder = $this->viewFolder;
        $viewData->subViewFolder = "image";

        $viewData->item_images = $this->image_model->get_all(
            array(
                "gallery_id"    => $id
            )
        );

        $viewData->item = $this->imageGallery_model->get(
            array(
                "id"    => $id
            )
        );

        $render_html = $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/render_elements/image_list_v", $viewData, true);

        echo $render_html;

    }

}
