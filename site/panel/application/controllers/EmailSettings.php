<?php
    
    class EmailSettings extends MY_Controller
    {
        public $viewFolder = "";
        
        public function __construct()
        {
            
            parent::__construct();
            
            $this->viewFolder = "emailSettings_v";
            
            $this->load->model("emailSettings_model");
            
            if (!get_active_user()) {
                redirect(base_url("login"));
            }
            
        }
        
        public function index()
        {
            $viewData = new stdClass();
            
            /** Tablodan Verilerin Getirilmesi.. */
            $items = $this->emailSettings_model->get_all(
                array()
            );
            
            /** View'e gönderilecek Değişkenlerin Set Edilmesi.. */
            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "list";
            $viewData->items = $items;
            
            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
        }
        
        public function new_form()
        {
            
            $viewData = new stdClass();
            
            /** View'e gönderilecek Değişkenlerin Set Edilmesi.. */
            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "add";
            
            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
            
        }
        
        public function save()
        {
            
            $this->load->library("form_validation");
            
            // Kurallar yazilir..
            
            $this->form_validation->set_rules("protocol", "Protokol Numarası", "required|trim");
            
            $this->form_validation->set_rules("host", "ePosta Sunucusu", "required|trim");
            
            $this->form_validation->set_rules("port", "Port Numarası", "required|trim");
            
            $this->form_validation->set_rules("user_name", "Gönderici Adı", "required|trim");
            
            $this->form_validation->set_rules("password", "ePosta Şifresi", "required|trim");
            
            $this->form_validation->set_rules("user", "ePosta (user)", "required|trim|valid_email");
            
            $this->form_validation->set_rules("from", "Kimden (from)", "required|trim|valid_email");
            
            $this->form_validation->set_rules("to", "Kime (to)", "required|trim|valid_email");
            
            $this->form_validation->set_message(
                array(
                    "required" => "<b>{field}</b> alanı boş bırakılamaz.",
                    "valid_email" => "Lütfen geçerli bir eposta adresi giriniz.",
                )
            );
            
            // Form Validation Calistirilir..
            $validate = $this->form_validation->run();
            
            if ($validate) {
                
                // Upload Süreci...
                
                $insert = $this->emailSettings_model->add(
                    array(
                        "protocol" => $this->input->post("protocol"),
                        "host" => $this->input->post("host"),
                        "port" => $this->input->post("port"),
                        "user_name" => $this->input->post("user_name"),
                        "password" => $this->input->post("password"),
                        "user" => $this->input->post("user"),
                        "from" => $this->input->post("from"),
                        "to" => $this->input->post("to"),
                        "isActive" => 1,
                        "createdAt" => date("Y-m-d H:i:s")
                    )
                );
                
                // TODO Alert sistemi eklenecek...
                if ($insert) {
                    
                    $alert = array(
                        "title" => "İşlem Başarılı",
                        "text" => "Kayıt başarılı bir şekilde eklendi",
                        "type" => "success"
                    );
                    
                    // İşlemin Sonucunu Session'a yazma işlemi...
                    $this->session->set_flashdata("alert", $alert);
                    
                    redirect(base_url("emailSettings"));
                    
                }
                else {
                    
                    $alert = array(
                        "title" => "İşlem Başarısız",
                        "text" => "Kayıt Ekleme sırasında bir problem oluştu",
                        "type" => "error"
                    );
                    
                    $this->session->set_flashdata("alert", $alert);
                    
                    redirect(base_url("emailSettings/new_form"));
                    
                    die();
                }
                // İşlemin Sonucunu Session'a yazma işlemi...
                $this->session->set_flashdata("alert", $alert);
                
                redirect(base_url("emailSettings"));
                
            }
            else {
                
                $viewData = new stdClass();
                
                /** View'e gönderilecek Değişkenlerin Set Edilmesi.. */
                $viewData->viewFolder = $this->viewFolder;
                $viewData->subViewFolder = "add";
                $viewData->form_error = true;
                
                $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
            }
            
        }
        
        public function update_form($id)
        {
            
            $viewData = new stdClass();
            
            /** Tablodan Verilerin Getirilmesi.. */
            $item = $this->emailSettings_model->get(
                array(
                    "id" => $id,
                )
            );
            
            /** View'e gönderilecek Değişkenlerin Set Edilmesi.. */
            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "update";
            $viewData->item = $item;
            
            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
            
        }
        
        public function update($id)
        {
            $this->load->library("form_validation");
            
            // Kurallar yazilir..
            
            $this->form_validation->set_rules("protocol", "Protokol Numarası", "required|trim");
            
            $this->form_validation->set_rules("host", "ePosta Sunucusu", "required|trim");
            
            $this->form_validation->set_rules("port", "Port Numarası", "required|trim");
            
            $this->form_validation->set_rules("user_name", "Gönderici Adı", "required|trim");
        
            $this->form_validation->set_rules("user", "ePosta (user)", "required|trim|valid_email");
    
            $this->form_validation->set_rules("from", "Kimden (from)", "required|trim|valid_email");
    
            $this->form_validation->set_rules("to", "Kime (to)", "required|trim|valid_email");
    
            $this->form_validation->set_rules("password", "ePosta Şifresi", "required|trim");
    
            $this->form_validation->set_message(
                array(
                    "required" => "<b>{field}</b> alanı boş bırakılamaz.",
                    "valid_email" => "Lütfen geçerli bir eposta adresi giriniz.",
                )
            );
            
            // Form Validation Calistirilir..
            $validate = $this->form_validation->run();
            
            if ($validate) {
                
                $update = $this->emailSettings_model->update(
                    array("id" => $id),
                    array(
                        "protocol" => $this->input->post("protocol"),
                        "host" => $this->input->post("host"),
                        "port" => $this->input->post("port"),
                        "user_name" => $this->input->post("user_name"),
                        "password" => $this->input->post("password"),
                        "user" => $this->input->post("user"),
                        "from" => $this->input->post("from"),
                        "to" => $this->input->post("to"),
                    )
            );
                
                
                // TODO Alert sistemi eklenecek...
                if ($update) {
                    
                    $alert = array(
                        "title" => "İşlem Başarılı",
                        "text" => "Kayıt başarılı bir şekilde güncellendi",
                        "type" => "success"
                    );
                    
                }
                else {
                    
                    $alert = array(
                        "title" => "İşlem Başarısız",
                        "text" => "Kayıt Güncelleme sırasında bir problem oluştu",
                        "type" => "error"
                    );
                }
                
                // İşlemin Sonucunu Session'a yazma işlemi...
                $this->session->set_flashdata("alert", $alert);
                
                redirect(base_url("emailSettings"));
                
            }
            else {
                
                $viewData = new stdClass();
                
                /** View'e gönderilecek Değişkenlerin Set Edilmesi.. */
                $viewData->viewFolder = $this->viewFolder;
                $viewData->subViewFolder = "update";
                $viewData->form_error = true;
                
                /** Tablodan Verilerin Getirilmesi.. */
                $viewData->item = $this->emailSettings_model->get(
                    array(
                        "id" => $id,
                    )
                );
                
                $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
            }
            
        }
        
        public function delete($id)
        {
            
            $delete = $this->emailSettings_model->delete(
                array(
                    "id" => $id
                )
            );
            
            // TODO Alert Sistemi Eklenecek...
            if ($delete) {
                
                $alert = array(
                    "title" => "İşlem Başarılı",
                    "text" => "Kayıt başarılı bir şekilde silindi",
                    "type" => "success"
                );
                
            }
            else {
                
                $alert = array(
                    "title" => "İşlem Başarılı",
                    "text" => "Kayıt silme sırasında bir problem oluştu",
                    "type" => "error"
                );
                
            }
            
            $this->session->set_flashdata("alert", $alert);
            redirect(base_url("emailSettings"));
            
        }
        
        public function isActiveSetter($id)
        {
            
            if ($id) {
                
                $isActive = ($this->input->post("data") === "true") ? 1 : 0;
                
                $this->emailSettings_model->update(
                    array(
                        "id" => $id
                    ),
                    array(
                        "isActive" => $isActive
                    )
                );
            }
        }
        
    }
