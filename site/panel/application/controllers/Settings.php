<?php

class Settings extends MY_Controller
{
    public $viewFolder = "";

    public function __construct()
    {

        parent::__construct();

        $this->viewFolder = "settings_v";

        $this->load->model("settings_model");

        if (!get_active_user()){
            redirect(base_url("login"));
        }

    }

    public function index()
    {
        $viewData = new stdClass();

        /** Tablodan Verilerin Getirilmesi.. */
        $item = $this->settings_model->get();

        if ($item)
            $viewData->subViewFolder = "update";
        else
            $viewData->subViewFolder = "no_content";


        /** View'e gönderilecek Değişkenlerin Set Edilmesi.. */
        $this->load->model("imageGallery_model");
        $viewData->galleries = $this->imageGallery_model->get_all(
            array(
                "isActive" => 1
            ), "gallery_name ASC"
        );

        $viewData->viewFolder = $this->viewFolder;
        $viewData->item = $item;

        $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
    }

    public function new_form()
    {

        $viewData = new stdClass();

        /** View'e gönderilecek Değişkenlerin Set Edilmesi.. */
        $viewData->viewFolder = $this->viewFolder;
        $viewData->subViewFolder = "add";

        $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);

    }

    public function save()
    {

        $this->load->library("form_validation");

        // Kurallar yazilir..

        if ($_FILES["logo"]["name"] == "") {

            $alert = array(
                "title" => "İşlem Başarısız",
                "text" => "Lütfen bir masaüstü logo seçiniz",
                "type" => "error"
            );

            // İşlemin Sonucunu Session'a yazma işlemi...
            $this->session->set_flashdata("alert", $alert);

            redirect(base_url("settings/new_form"));

            die();
        }

        if ($_FILES["logo_mobile"]["name"] == "") {

            $alert = array(
                "title" => "İşlem Başarısız",
                "text" => "Lütfen bir mobil logo seçiniz",
                "type" => "error"
            );

            // İşlemin Sonucunu Session'a yazma işlemi...
            $this->session->set_flashdata("alert", $alert);

            redirect(base_url("settings/new_form"));

            die();
        }

        if ($_FILES["favicon"]["name"] == "") {

            $alert = array(
                "title" => "İşlem Başarısız",
                "text" => "Lütfen bir favicon seçiniz",
                "type" => "error"
            );

            // İşlemin Sonucunu Session'a yazma işlemi...
            $this->session->set_flashdata("alert", $alert);

            redirect(base_url("settings/new_form"));

            die();
        }

        $this->form_validation->set_rules("company_name", "Şirket Adı", "required|trim");
        $this->form_validation->set_rules("phone_1", "Telefon 1", "required|trim");
        $this->form_validation->set_rules("latitude", "Enlem (Latitude)", "required|trim");
        $this->form_validation->set_rules("longtitude", "Boylam (Longtitude)", "required|trim");
        $this->form_validation->set_rules("email", "ePosta Adresi", "required|trim|valid_email");

        $this->form_validation->set_message(
            array(
                "required" => "<b>{field}</b> alanı boş bırakılamaz.",
                "valid_email"   => "Lütfen geçerli bir ePosta adresi giriniz."
            )
        );

        // Form Validation Calistirilir..
        $validate = $this->form_validation->run();

        if ($validate) {

            // Upload Süreci...

            $file_name = convertToSEO($this->input->post("company_name")) . "." . pathinfo($_FILES["logo"]["name"], PATHINFO_EXTENSION);

            $image_150x35 = upload_picture($_FILES["logo"]["tmp_name"], "uploads/$this->viewFolder", 150, 35, $file_name);
            $image_140x33 = upload_picture($_FILES["logo_mobile"]["tmp_name"], "uploads/$this->viewFolder", 140, 33, $file_name);
            $image_192x192 = upload_picture($_FILES["favicon"]["tmp_name"], "uploads/$this->viewFolder", 192, 192, $file_name);
            $image_90x21 = upload_picture($_FILES["logo"]["tmp_name"], "uploads/$this->viewFolder", 90, 21, $file_name);

            if ($image_150x35 && $image_90x21 && $image_140x33 && $image_192x192) {

                $insert = $this->settings_model->add(
                    array(
                        "company_name" => $this->input->post("company_name"),
                        "phone_1" => $this->input->post("phone_1"),
                        "phone_2" => $this->input->post("phone_2"),
                        "fax_1" => $this->input->post("fax_1"),
                        "fax_2" => $this->input->post("fax_2"),
                        "address" => $this->input->post("address"),
                        "latitude" => $this->input->post("latitude"),
                        "longtitude" => $this->input->post("longtitude"),
                        "bbox" => $this->input->post("bbox"),
                        "about_us" => $this->input->post("about_us"),
                        "mission" => $this->input->post("mission"),
                        "vision" => $this->input->post("vision"),
                        "email" => $this->input->post("email"),
                        "facebook" => $this->input->post("facebook"),
                        "twitter" => $this->input->post("twitter"),
                        "instagram" => $this->input->post("instagram"),
                        "linkedin" => $this->input->post("linkedin"),
                        "github" => $this->input->post("github"),
                        "gitlab" => $this->input->post("gitlab"),
                        "skype" => $this->input->post("skype"),
                        "googleplus" => $this->input->post("googleplus"),
                        "whatsapp" => $this->input->post("whatsapp"),
                        "homepage_gallery" => $this->input->post("homepage_gallery"),
                        "logo" => $file_name,
                        "logo_mobile" => $file_name,
                        "favicon" => $file_name,
                        "createdAt" => date("Y-m-d H:i:s")
                    )
                );

                // TODO Alert sistemi eklenecek...
                if ($insert) {

                    $alert = array(
                        "title" => "İşlem Başarılı",
                        "text" => "Kayıt başarılı bir şekilde eklendi",
                        "type" => "success"
                    );

                } else {

                    $alert = array(
                        "title" => "İşlem Başarısız",
                        "text" => "Kayıt Ekleme sırasında bir problem oluştu",
                        "type" => "error"
                    );
                }

            } else {

                $alert = array(
                    "title" => "İşlem Başarısız",
                    "text" => "Görsel yüklenirken bir problem oluştu",
                    "type" => "error"
                );

                $this->session->set_flashdata("alert", $alert);

                redirect(base_url("settings/new_form"));

                die();

            }

            // İşlemin Sonucunu Session'a yazma işlemi...
            $this->session->set_flashdata("alert", $alert);

            redirect(base_url("settings"));

        } else {

            $viewData = new stdClass();

            /** View'e gönderilecek Değişkenlerin Set Edilmesi.. */
            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "add";
            $viewData->form_error = true;

            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
        }

    }

    public function update_form($id)
    {

        $viewData = new stdClass();

        /** Tablodan Verilerin Getirilmesi.. */
        $item = $this->settings_model->get(
            array(
                "id" => $id,
            )
        );

        /** View'e gönderilecek Değişkenlerin Set Edilmesi.. */
        $viewData->viewFolder = $this->viewFolder;
        $viewData->subViewFolder = "update";
        $viewData->item = $item;

        $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);

    }

    public function update($id)
    {

        $this->load->library("form_validation");

        // Kurallar yazilir..

        $this->form_validation->set_rules("company_name", "Şirket Adı", "required|trim");
        $this->form_validation->set_rules("phone_1", "Telefon 1", "required|trim");
        $this->form_validation->set_rules("email", "ePosta Adresi", "required|trim|valid_email");

        $this->form_validation->set_message(
            array(
                "required" => "<b>{field}</b> alanı boş bırakılamaz.",
                "valid_email"   => "Lütfen geçerli bir ePosta adresi giriniz."
            )
        );

        // Form Validation Calistirilir..
        $validate = $this->form_validation->run();

        if ($validate) {

            $data = array(
                "company_name" => $this->input->post("company_name"),
                "phone_1" => $this->input->post("phone_1"),
                "phone_2" => $this->input->post("phone_2"),
                "fax_1" => $this->input->post("fax_1"),
                "fax_2" => $this->input->post("fax_2"),
                "address" => $this->input->post("address"),
                "latitude" => $this->input->post("latitude"),
                "longtitude" => $this->input->post("longtitude"),
                "bbox" => $this->input->post("bbox"),
                "about_us" => $this->input->post("about_us"),
                "mission" => $this->input->post("mission"),
                "vision" => $this->input->post("vision"),
                "email" => $this->input->post("email"),
                "facebook" => $this->input->post("facebook"),
                "twitter" => $this->input->post("twitter"),
                "instagram" => $this->input->post("instagram"),
                "linkedin" => $this->input->post("linkedin"),
                "github" => $this->input->post("github"),
                "gitlab" => $this->input->post("gitlab"),
                "skype" => $this->input->post("skype"),
                "googleplus" => $this->input->post("googleplus"),
                "whatsapp" => $this->input->post("whatsapp"),
                "homepage_gallery" => $this->input->post("homepage_gallery"),
                "updatedAt" => date("Y-m-d H:i:s")
            );

            // Masaüstü Logo görseli için upload süreci
            if ($_FILES["logo"]["name"] !== "") {

                $file_name = convertToSEO($this->input->post("company_name")) . "." . pathinfo($_FILES["logo"]["name"], PATHINFO_EXTENSION);

                $image_150x35 = upload_picture($_FILES["logo"]["tmp_name"], "uploads/$this->viewFolder", 150, 35, $file_name);
                $image_90x21 = upload_picture($_FILES["logo"]["tmp_name"], "uploads/$this->viewFolder", 90, 21, $file_name);

                if ($image_150x35 && $image_90x21) {

                        $data["logo"] = $file_name;

                } else {

                    $alert = array(
                        "title" => "İşlem Başarısız",
                        "text" => "Masaüstü logo görseli yüklenirken bir problem oluştu",
                        "type" => "error"
                    );

                    $this->session->set_flashdata("alert", $alert);

                    redirect(base_url("settings/update_form/$id"));

                    die();

                }

            }

            // Mobil Logo görseli için upload süreci
            if ($_FILES["logo_mobile"]["name"] !== "") {

                $file_name = convertToSEO($this->input->post("company_name")) . "." . pathinfo($_FILES["logo_mobile"]["name"], PATHINFO_EXTENSION);

                $image_140x33 = upload_picture($_FILES["logo_mobile"]["tmp_name"], "uploads/$this->viewFolder", 140, 33, $file_name);

                if ($image_140x33) {

                    $data["logo_mobile"] = $file_name;

                } else {

                    $alert = array(
                        "title" => "İşlem Başarısız",
                        "text" => "Mobil logo görseli yüklenirken bir problem oluştu",
                        "type" => "error"
                    );

                    $this->session->set_flashdata("alert", $alert);

                    redirect(base_url("settings/update_form/$id"));

                    die();

                }

            }

            // Favicon görseli için upload süreci
            if ($_FILES["favicon"]["name"] !== "") {

                $file_name = convertToSEO($this->input->post("company_name")) . "." . pathinfo($_FILES["favicon"]["name"], PATHINFO_EXTENSION);

                $image_192x192 = upload_picture($_FILES["favicon"]["tmp_name"], "uploads/$this->viewFolder", 192, 192, $file_name);

                if ($image_192x192) {

                    $data["favicon"] = $file_name;

                } else {

                    $alert = array(
                        "title" => "İşlem Başarısız",
                        "text" => "Favicon görseli yüklenirken bir problem oluştu",
                        "type" => "error"
                    );

                    $this->session->set_flashdata("alert", $alert);

                    redirect(base_url("settings/update_form/$id"));

                    die();

                }

            }

            $update = $this->settings_model->update(array("id" => $id), $data);

            // TODO Alert sistemi eklenecek...
            if ($update) {

                $alert = array(
                    "title" => "İşlem Başarılı",
                    "text" => "Kayıt başarılı bir şekilde güncellendi",
                    "type" => "success"
                );

            } else {

                $alert = array(
                    "title" => "İşlem Başarısız",
                    "text" => "Kayıt Güncelleme sırasında bir problem oluştu",
                    "type" => "error"
                );
            }

            // Session set işlemi

            $settings = $this->settings_model->get();
            $this->session->set_userdata("settings", $settings);

            // İşlemin Sonucunu Session'a yazma işlemi...
            $this->session->set_flashdata("alert", $alert);

            redirect(base_url("settings"));

        } else {


            $viewData = new stdClass();

            /** View'e gönderilecek Değişkenlerin Set Edilmesi.. */
            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "update";
            $viewData->form_error = true;

            /** Tablodan Verilerin Getirilmesi.. */
            $viewData->item = $this->settings->get(
                array(
                    "id" => $id,
                )
            );

            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
        }

    }

}
