<?php
    
    class Users extends MY_Controller
    {
        public $viewFolder = "";
        
        public function __construct()
        {
            
            parent::__construct();
            
            $this->viewFolder = "users_v";
            
            $this->load->model("user_model");
            $this->load->model("userRole_model");
            
            if (!get_active_user()){
                redirect(base_url("login"));
            }
            
        }
        
        public function index()
        {
            $viewData = new stdClass();

            $user = get_active_user();

            if (isAdmin())
            {
                $where = array();
            } else {
                $where = array(
                    "id"    => $user->id
                );
            }
            
            /** Tablodan Verilerin Getirilmesi.. */
            $items = $this->user_model->get_all(
                $where
            );
            
            /** View'e gönderilecek Değişkenlerin Set Edilmesi.. */
            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "list";
            $viewData->items = $items;
            
            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
        }
        
        public function new_form()
        {
            
            $viewData = new stdClass();

            $viewData->user_roles = $this->userRole_model->get_all(
                array(
                    "isActive"  => 1
                )
            );

            /** View'e gönderilecek Değişkenlerin Set Edilmesi.. */
            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "add";
            
            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
            
        }
        
        public function save()
        {
            
            $this->load->library("form_validation");
            
            // Kurallar yazilir..
            
            $this->form_validation->set_rules("user_name", "Kullanıcı Adı", "required|trim|is_unique[users.user_name]");
            $this->form_validation->set_rules("full_name", "Ad Soyad", "required|trim");
            $this->form_validation->set_rules("email", "ePosta Adresi", "required|trim|valid_email|is_unique[users.email]");
            $this->form_validation->set_rules("user_role_id", "Kullanıcı Rolü", "required|trim]");
            $this->form_validation->set_rules("password", "Şifre", "required|trim|min_length[8]");
            $this->form_validation->set_rules("re_password", "Şifre Doğrulama", "required|trim|matches[password]");
            
            $this->form_validation->set_message(
                array(
                    "required" => "<b>{field}</b> alanı boş bırakılamaz.",
                    "valid_email" => "Lütfen geçerli bir eposta adresi giriniz.",
                    "is_unique" => "Bu <b>{field}</b> alınmış. Lütfen başka bir alternatif deneyiniz.",
                    "matches" => "Bu şifreler eşleşmiyor.Lütfen tekrar deneyiniz.",
                    "min_length" => "Şifre en az <b>8</b> karakter olmalıdır"
                )
            );
            
            // Form Validation Calistirilir..
            $validate = $this->form_validation->run();
            
            if ($validate) {
                
                // Upload Süreci...
                
                $file_name = convertToSEO(pathinfo($_FILES["img_url"]["name"], PATHINFO_FILENAME)) . "." . pathinfo($_FILES["img_url"]["name"], PATHINFO_EXTENSION);
                
                $config["allowed_types"] = "jpg|jpeg|png";
                $config["upload_path"] = "uploads/$this->viewFolder/";
                $config["file_name"] = $file_name;
                
                $this->load->library("upload", $config);
                
                $upload = $this->upload->do_upload("img_url");
                
                if ($upload) {
                    
                    $uploaded_file = $this->upload->data("file_name");
                    
                    $insert = $this->user_model->add(
                        array(
                            "user_name" => $this->input->post("user_name"),
                            "full_name" => $this->input->post("full_name"),
                            "email" => $this->input->post("email"),
                            "user_role_id" => $this->input->post("user_role_id"),
                            "password" => md5($this->input->post("password")),
                            "img_url" => $uploaded_file,
                            "isActive" => 1,
                            "createdAt" => date("Y-m-d H:i:s")
                        )
                    );
                    
                    // TODO Alert sistemi eklenecek...
                    if ($insert) {
                        
                        $alert = array(
                            "title" => "İşlem Başarılı",
                            "text"  => "Kayıt başarılı bir şekilde eklendi",
                            "type"  => "success"
                        );
                        
                    }
                    else {
                        
                        $alert = array(
                            "title" => "İşlem Başarısız",
                            "text"  => "Kayıt Ekleme sırasında bir problem oluştu",
                            "type"  => "error"
                        );
                    }
                    
                }
                else {
                    
                    $alert = array(
                        "title" => "İşlem Başarısız",
                        "text" => "Görsel yüklenirken bir problem oluştu",
                        "type" => "error"
                    );
                    
                    $this->session->set_flashdata("alert", $alert);
                    
                    redirect(base_url("users/new_form"));
                    
                    die();
                    
                }
                
                // İşlemin Sonucunu Session'a yazma işlemi...
                $this->session->set_flashdata("alert", $alert);
                
                redirect(base_url("users"));
                
            }
            else {
                
                $viewData = new stdClass();
                
                /** View'e gönderilecek Değişkenlerin Set Edilmesi.. */
                $viewData->viewFolder = $this->viewFolder;
                $viewData->subViewFolder = "add";
                $viewData->form_error = true;
                
                $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
            }
            
        }
        
        public function update_form($id)
        {
            
            $viewData = new stdClass();
            
            /** Tablodan Verilerin Getirilmesi.. */
            $item = $this->user_model->get(
                array(
                    "id" => $id,
                )
            );

            $viewData->user_roles = $this->userRole_model->get_all(
                array(
                    "isActive"  => 1
                )
            );
            
            /** View'e gönderilecek Değişkenlerin Set Edilmesi.. */
            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "update";
            $viewData->item = $item;
            
            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
            
        }

        public function update($id)
        {
            $this->load->library("form_validation");

            $oldUser = $this->user_model->get(
                array(
                    "id" => $id
                )
            );

            if ($oldUser->user_name != $this->input->post("user_name")) {

                $this->form_validation->set_rules("user_name", "Kullanıcı Adı", "required|trim|is_unique[users.user_name]");
                $this->form_validation->set_rules("user_role_id", "Kullanıcı Rolü", "required|trim]");

            }

            if ($oldUser->email != $this->input->post("email")) {

                $this->form_validation->set_rules("email", "ePosta Adresi", "required|trim|valid_email|is_unique[users.email]");

            }

            // Kurallar yazilir..

            $this->form_validation->set_rules("full_name", "Ad Soyad", "required|trim");

            $this->form_validation->set_message(
                array(
                    "required" => "<b>{field}</b> alanı boş bırakılamaz.",
                    "valid_email" => "Lütfen geçerli bir eposta adresi giriniz.",
                    "is_unique" => "Bu <b>{field}</b> alınmış. Lütfen başka bir alternatif deneyiniz.",
                )
            );

            // Form Validation Calistirilir..
            $validate = $this->form_validation->run();

            if ($validate) {

                if ($_FILES["img_url"]["name"] !== "") {

                    unlink("uploads/{$this->viewFolder}/$oldUser->img_url");

                    $file_name = convertToSEO(pathinfo($_FILES["img_url"]["name"], PATHINFO_FILENAME)) . "." . pathinfo($_FILES["img_url"]["name"], PATHINFO_EXTENSION);

                    $config["allowed_types"] = "jpg|jpeg|png";
                    $config["upload_path"] = "uploads/$this->viewFolder/";
                    $config["file_name"] = $file_name;

                    $this->load->library("upload", $config);

                    $upload = $this->upload->do_upload("img_url");

                    if ($upload) {

                        $uploaded_file = $this->upload->data("file_name");

                        $data = array(
                            "user_name" => $this->input->post("user_name"),
                            "full_name" => $this->input->post("full_name"),
                            "email" => $this->input->post("email"),
                            "user_role_id" => $this->input->post("user_role_id"),
                            "img_url" => $uploaded_file,
                        );

                    }
                    else {

                        $alert = array(
                            "title" => "İşlem Başarısız",
                            "text" => "Görsel yüklenirken bir problem oluştu",
                            "type" => "error"
                        );

                        $this->session->set_flashdata("alert", $alert);

                        redirect(base_url("users/update_form/$id"));

                        die();

                    }

                }
                else {

                    $data = array(
                        "user_name" => $this->input->post("user_name"),
                        "full_name" => $this->input->post("full_name"),
                        "email" => $this->input->post("email"),
                        "user_role_id" => $this->input->post("user_role_id")
                    );

                }

                $update = $this->user_model->update(array("id" => $id), $data);

                // TODO Alert sistemi eklenecek...
                if ($update) {

                    $alert = array(
                        "title" => "İşlem Başarılı",
                        "text" => "Kayıt başarılı bir şekilde güncellendi",
                        "type" => "success"
                    );

                }
                else {

                    $alert = array(
                        "title" => "İşlem Başarısız",
                        "text" => "Kayıt Güncelleme sırasında bir problem oluştu",
                        "type" => "error"
                    );
                }

                // İşlemin Sonucunu Session'a yazma işlemi...
                $this->session->set_flashdata("alert", $alert);

                redirect(base_url("users"));

            }
            else {

                $viewData = new stdClass();

                /** View'e gönderilecek Değişkenlerin Set Edilmesi.. */
                $viewData->viewFolder = $this->viewFolder;
                $viewData->subViewFolder = "update";
                $viewData->form_error = true;

                /** Tablodan Verilerin Getirilmesi.. */
                $viewData->item = $this->user_model->get(
                    array(
                        "id" => $id,
                    )
                );

                $viewData->user_roles = $this->userRole_model->get_all(
                    array(
                        "isActive"  => 1
                    )
                );

                $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
            }

        }

        public function update_password_form($id)
        {

            $viewData = new stdClass();

            /** Tablodan Verilerin Getirilmesi.. */
            $item = $this->user_model->get(
                array(
                    "id" => $id,
                )
            );

            /** View'e gönderilecek Değişkenlerin Set Edilmesi.. */
            $viewData->viewFolder = $this->viewFolder;
            $viewData->subViewFolder = "password";
            $viewData->item = $item;

            $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);

        }

        public function update_password($id)
        {
            
            $this->load->library("form_validation");
            
            $this->form_validation->set_rules("password", "Şifre", "required|trim|min_length[6]");
            $this->form_validation->set_rules("re_password", "Şifre Doğrula", "required|trim|min_length[6]|matches[password]");
            
            $this->form_validation->set_message(
                array(
                    "required"      =>  "<b>{field}</b> alanı boş bırakılamaz.",
                    "matches"       =>  "Bu şifreler eşleşmiyor.Lütfen tekrar deneyiniz.",
                    "min_length"    =>  "Şifre 6 karakterden az olamaz"
                )
            );
            
            // Form Validation Calistirilir..
            $validate = $this->form_validation->run();
            
            if ($validate) {
                
                // Upload Süreci...
                $update = $this->user_model->update(
                    array("id" => $id),
                    array(
                        "password" => md5($this->input->post("password")),
                    )
                );
                
                // TODO Alert sistemi eklenecek...
                if ($update) {
                    
                    $alert = array(
                        "title" => "İşlem Başarılı",
                        "text" => "Şifreniz başarılı bir şekilde güncellendi",
                        "type" => "success"
                    );
                    
                }
                else {
                    
                    $alert = array(
                        "title" => "İşlem Başarısız",
                        "text" => "Şifre Güncelleme sırasında bir problem oluştu",
                        "type" => "error"
                    );
                }
                
                // İşlemin Sonucunu Session'a yazma işlemi...
                $this->session->set_flashdata("alert", $alert);
                
                redirect(base_url("users"));
                
            }
            else {
                
                $viewData = new stdClass();
                
                /** View'e gönderilecek Değişkenlerin Set Edilmesi.. */
                $viewData->viewFolder = $this->viewFolder;
                $viewData->subViewFolder = "password";
                $viewData->form_error = true;
                
                /** Tablodan Verilerin Getirilmesi.. */
                $viewData->item = $this->user_model->get(
                    array(
                        "id" => $id,
                    )
                );
                
                $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
            }
            
        }

        public function delete($id)
        {
            
            $delete = $this->user_model->delete(
                array(
                    "id" => $id
                )
            );
            
            // TODO Alert Sistemi Eklenecek...
            if ($delete) {
                
                $alert = array(
                    "title" => "İşlem Başarılı",
                    "text" => "Kayıt başarılı bir şekilde silindi",
                    "type" => "success"
                );
                
            }
            else {
                
                $alert = array(
                    "title" => "İşlem Başarılı",
                    "text" => "Kayıt silme sırasında bir problem oluştu",
                    "type" => "error"
                );
                
            }
            
            $this->session->set_flashdata("alert", $alert);
            redirect(base_url("users"));
            
        }
        
        public function isActiveSetter($id)
        {
            
            if ($id) {
                
                $isActive = ($this->input->post("data") === "true") ? 1 : 0;
                
                $this->user_model->update(
                    array(
                        "id" => $id
                    ),
                    array(
                        "isActive" => $isActive
                    )
                );
            }
        }
        
    }
